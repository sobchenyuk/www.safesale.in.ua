(() => {

    let AliasChange = function () {

        let alias = document.querySelector('#alias');
        let title = document.querySelector('#title');
        let transliterate = (
            function() {
                var
                    rus = "щ   ш  ч  ц  ю  я  ё  ж  ъ  ы  э  а б в г д е з и й к л м н о п р с т у ф х ь".split(/ +/g),
                    eng = "shh sh ch cz yu ya yo zh _ s e a b v g d e z i j k l m n o p r s t u f x _".split(/ +/g)
                ;
                return function(text, engToRus) {
                    var x;
                    for(x = 0; x < rus.length; x++) {
                        text = text.split(engToRus ? eng[x] : rus[x]).join(engToRus ? rus[x] : eng[x]);
                        text = text.split(engToRus ? eng[x].toUpperCase() : rus[x].toUpperCase()).join(engToRus ? rus[x].toUpperCase() : eng[x].toUpperCase());
                    }
                    return text;
                }
            }
        )();

        function input(e) {
            let str = transliterate(this.value.toLowerCase());
            let rez = str.length;
            let i;
            let arr = [];

            for( i = 0; i < rez; i++ ){
                str.replace(/\s/i, '_')
                arr[i] = str[i].replace(/\s/i, '_')
            }

            alias.setAttribute('value', arr.join(''))

        }

        return {
            handler: function () {
                if (alias) {
                    title.addEventListener('input', input);
                }
            }
        }

    };

    window.addEventListener('load', run);

    function run(e) {

        let module = new AliasChange();
        module.handler();

    }

})()