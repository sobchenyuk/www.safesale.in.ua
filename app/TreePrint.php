<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TreePrint extends Model
{
    //
    static public function treePrint($tree, $pid=0) {
        if (empty($tree[$pid]))
            return;
        echo '<ul>';
        foreach ($tree[$pid] as $k => $row) {
            echo <<<HTML
            <li class="">
<a class="regionlink region_link" data-id="24">
<span>{$row->title}</span>
</a>
HTML;
            if (isset($tree[$row->id]))
                TreePrint::treePrint($tree, $row->id);
            echo '</li>';
        }
        echo '</ul>';
    }
}
