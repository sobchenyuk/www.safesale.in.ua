<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\DB;

class PersonaAccount extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @return Response
     */

    public function showHome()
    {
        return View('personal-account.index', array('page' => 'personal-account', 'title' => 'Активные объявления'));
    }


    public function getParam(){

        if(isset($_POST['name'])){

            $id = Auth::user()->id;
            $dateTrue = date('Y-m-d');
            $time = DB::table('users')->where('id', $id)->first();
            $timeTrue = preg_replace('/\s([0-9]{0,2})\:([0-9]{0,2})\:([0-9]{0,2})$/i', '', $time->updated_at);

            if($dateTrue == $timeTrue){
                echo "<script>;
window.addEventListener('DOMContentLoaded', function() {
  let message = document.querySelector('#formChange #allert');
    message.classList.add('error');
    message.style.display = 'block';
    message.innerHTML = 'Нельзя менять данные чаще чем раз в сутки';
});
</script>";
            }else {
                $name = trim(urldecode(htmlspecialchars(($_POST['name']))));
                $surname = trim(urldecode(htmlspecialchars(($_POST['surname']))));
                $region = trim(urldecode(htmlspecialchars(($_POST['region']))));
                $area = trim(urldecode(htmlspecialchars(($_POST['area']))));
                $email = trim(urldecode(htmlspecialchars(($_POST['email']))));
                $password = bcrypt(trim(urldecode(htmlspecialchars(($_POST['password'])))));
                $date = date('Y-m-d h:i:s');

                DB::table('users')
                    ->where('id', $id)
                    ->update([
                        'name' => $name,
                        'surname' => $surname,
                        'region' => $region,
                        'area' => $area,
                        'password' => $password,
                        'email' => $email,
                        'updated_at' => $date
                    ]);

                echo "<script>;
window.addEventListener('DOMContentLoaded', function() {
  let buttonUserName = document.querySelector('#buttonUserName span');
  let message = document.querySelector('#formChange #allert');
    message.style.display = 'block';
    message.innerHTML = 'Изменения прошли успешно, на Ваш email отправлено письмо с контактной информацией';
  buttonUserName.innerHTML = '$name';
});
</script>";
            }
        }
    }

    public function tabView($id)
    {
        switch ($id) {
            case "your-bets":
                return View('personal-account.index', array('page' => $id, 'title' => 'Ваши ставки'));
                break;
            case "messages":
                return View('personal-account.index', array('page' => $id, 'title' => 'Сообщения'));
                break;
            case "your-purchase":
                return View('personal-account.index', array('page' => $id, 'title' =>'Ваши покупки'));
                break;
            case "archive":
                return View('personal-account.index', array('page' => $id, 'title' => 'Архив'));
                break;
            case "settings":
                $this->getParam();
                $email = Auth::user()->email;
                $user = DB::table('users')->where('email', $email)->first();
                return View('personal-account.index', array(
                    'page' => $id,
                    'title' => 'Настройки',
                    'users' => $user
                ));
                break;
            default;
                abort( 404);
                break;
        }
    }
}