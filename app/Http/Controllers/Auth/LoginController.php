<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Zizaco\Entrust\Entrust;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     * @return string
     */

    public function redirectPath()
    {
        if (method_exists($this, 'redirectTo')) {
            return $this->redirectTo();
        }
        if (Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Moderator')) {
            return property_exists($this, 'redirectTo') ? $this->redirectTo : '/admin';
        } else {
            return property_exists($this, 'redirectTo') ? $this->redirectTo : '/personal-account';
        }
    }

    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {

        $this->middleware('guest')->except('logout');
    }
}
