<?php

namespace App\Http\Controllers\Auth;

use App\Role;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;



    protected $UserDate;


    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/entrance';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'surname' => 'required|max:255',
            'region' => 'required|max:255',
            'area' => 'required|max:255',
            'phone' => 'required|max:255',
            'captcha' => 'required|captcha',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $this->UserDate = $data;

        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'surname' => $data['surname'],
            'region' => $data['region'],
            'area' => $data['area'],
            'phone' => $data['phone']
        ]);
    }


    public function registered(Request $request)
    {
        //Настроить почту
        /*Mail::send('emails.registration', array(
            'name' => $this->UserDate['name'],
            'email' => $this->UserDate['email'],
            'password' => $this->UserDate['password'],
            'surname' => $this->UserDate['surname'],
            'region' => $this->UserDate['region'],
            'area' => $this->UserDate['area'],
            'phone' => $this->UserDate['phone']
        ), function($message) use ($request)
        {
            $message->from('us@example.com', 'Laravel');

            $message->to(
                $this->UserDate['email']
            )->subject('Регистрация на сайте ');
        });*/

        $email = $request->input('email');

        $userRole = Role::where('name', 'User')->first();
        $user = User::where('email', '=', $email)->first();

        $user->roles()->attach($userRole->id);

        $html = "<script >
        alert('Регистрация прошла успешно, на Ваш email отправлено письмо с контактной информацией');
        document.location.href = '/login';
</script>";

        return $html;
    }


}
