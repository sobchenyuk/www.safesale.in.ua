<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ShowHome extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @return Response
     */
//    public function __invoke()
//    {
////        Schema::table('pages', function(Blueprint $table)
////        {
////            $table->string('alias')->after('id');
////        });
//        if(){
//
//        }
//
//        return view('home.index');
//    }

    public function __invoke()
    {
        return view('home.index');
    }

    public function pages()
    {
        $url = url()->current();
        preg_match_all('/\w*$/',$url, $out, PREG_SET_ORDER);
        $alias =  $out[0][0];
        $pages = DB::table('pages')->where('alias', $alias)->first();

        return view('pages.index',
            [
                'title' => $pages->title,
                'short' => $pages->short,
            ]);
    }

}