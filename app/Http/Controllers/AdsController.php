<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function __invoke()
    {
        //Категории
        $categorys = DB::table('category')->get();

        $tree = array();
        foreach ($categorys as $row) {
            $tree[(int) $row->parent_id][] = $row;
        }


        return view('ad.index', [ 'tree' => $tree ]);
    }

}
