<?php

use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(\App\Models\Region::class, function (ModelConfiguration $model) {
    $model->setTitle('Области');

    $model->onDisplay(function (){

        $display = AdminDisplay::table();

        $display->paginate(15);

        $display->setHtmlAttribute('class', 'table-bordered table-primary table-hover');

        $display->setColumns([
            AdminColumnEditable::text('title')->setLabel('Область'),
        ]);

        return $display;
    });


    $model->onEdit(function($id) {

        $form = AdminForm::form()->setElements([
            AdminFormElement::text('title', 'Название области')->required()->unique(),
        ]);

        return $form;
    });

    $model->onCreate(function($id = null) {

        $form = AdminForm::form()->setElements([
            AdminFormElement::text('title', 'Название области')->required()->unique(),
        ]);

        return $form;
    });
});