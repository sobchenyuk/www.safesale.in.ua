<?php

use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(\App\Models\Area::class, function (ModelConfiguration $model) {
    $model->setTitle('Районы городов');

    $model->onDisplay(function (){

        $display = AdminDisplay::table();

        $display->paginate(15);

        $display->setHtmlAttribute('class', 'table-bordered table-primary table-hover');

        $display->setColumns([
            AdminColumnEditable::text('title')->setLabel('Район'),
        ]);

        return $display;
    });


    $model->onEdit(function($id) {

        $form = AdminForm::form()->setElements([
            AdminFormElement::text('title', 'Название района')->required()->unique(),
        ]);

        return $form;
    });

    $model->onCreate(function($id = null) {

        $form = AdminForm::form()->setElements([
            AdminFormElement::text('title', 'Название района')->required()->unique(),
        ]);

        return $form;
    });
});