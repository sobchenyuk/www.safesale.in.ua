<?php


use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(\App\Models\Pages::class, function (ModelConfiguration $model){
    $model->setTitle('Страницы');


    $model->onDisplay(function (){
        $display = AdminDisplay::table();

        $display->setHtmlAttribute('class', 'table-bordered table-primary table-hover');

        $display->paginate(15);

        $display->setColumns([
            AdminColumn::link('title')->setLabel('Название страницы'),
            AdminColumnEditable::text('alias')->setLabel('SEO Link'),
            AdminColumnEditable::checkbox('visible','Опублековано', 'Не опублековано')
                ->setLabel('Публикация сраниц'),
            AdminColumn::text('position', 'Позиция'),
        ]);
        return $display;
    });

    $model->onCreate(function($id = null) {
        $form = AdminForm::panel();

            $form->addBody([
                AdminDisplay::tabbed([
                    'Создание страницы' => new \SleepingOwl\Admin\Form\FormElements([
                        AdminFormElement::columns([
                            [
                                AdminFormElement::text('alias', 'Ссылка')
                                    ->required('Поле "Ссылка" обязетельно для заполнения')
                                    ->unique('Поле должно быть уникальным'),
                                AdminFormElement::text('title', 'Заголовок')
                                    ->required('Поле "Значение" обязетельно для заполнения'),
                                AdminFormElement::select('position','Позиция',[
                                    'Футер левая' => 'Футер левая',
                                    'Футер правая' => 'Футер правая',
                                    'Позиция копирайта' => 'Позиция копирайта',
                                    'Без позиции' => 'Без позиции',
                                ])
                                    ->required('Поле "Позиция" обязетельно для заполнения'),
                                AdminFormElement::select('visible','Публикация сраниц',[
                                    1 => 'Опублековано',
                                    0 => 'Не опублековано',
                                ])
                                    ->setDefaultValue('1')
                            ]
                        ]),
                        AdminFormElement::wysiwyg('short', 'Текст страницы')
                    ])
                ])
            ]);

        return $form;
    });

    $model->onEdit(function($id = null) {
        $form = AdminForm::panel();

            $form->addBody([
                AdminDisplay::tabbed([
                    'Создание страницы' => new \SleepingOwl\Admin\Form\FormElements([
                        AdminFormElement::columns([
                            [
                                AdminFormElement::text('alias', 'Ссылка')
                                    ->required('Поле "Ссылка" обязетельно для заполнения'),
                                AdminFormElement::text('title', 'Заголовок')
                                    ->required('Поле "Значение" обязетельно для заполнения'),
                                AdminFormElement::select('position','Позиция',[
                                    'Футер левая' => 'Футер левая',
                                    'Футер правая' => 'Футер правая',
                                    'Позиция копирайта' => 'Позиция копирайта',
                                    'Без позиции' => 'Без позиции',
                                ])
                                    ->required('Поле "Позиция" обязетельно для заполнения'),
                                AdminFormElement::select('visible','Публикация сраниц',[
                                    1 => 'Опублековано',
                                    0 => 'Не опублековано',
                                ])
                                    ->setDefaultValue('1')
                            ]
                        ]),
                        AdminFormElement::wysiwyg('short', 'Текст страницы')
                    ])
                ])
            ]);

        return $form;
    });
});