<?php


use SleepingOwl\Admin\Navigation\Page;

return [
    [
        'title' => 'Information',
        'icon'  => 'fa fa-exclamation-circle',
        'url'   => route('admin.information'),
    ],
    (new Page(\App\Models\Users::class))->setIcon('fa fa-users')->setPriority(100),
    (new Page(\App\Models\Pages::class))->setIcon('fa fa-file-o')->setPriority(100),
    [
        'title' => 'Справочники',
        'icon' => 'fa fa-lightbulb-o',
        'pages' => [
            (new Page(\App\Models\Category::class))->setIcon('fa fa-cubes')->setPriority(100),
            (new Page(\App\Models\Town::class))->setIcon('fa fa-hospital-o')->setPriority(300),
            (new Page(\App\Models\Region::class))->setIcon('fa fa-pie-chart')->setPriority(200),
            (new Page(\App\Models\Area::class))->setIcon('fa fa-pie-chart')->setPriority(400),
        ]
    ],
    [
        'title' => 'Рассылка',
        'icon'  => 'fa fa-envelope',
        'priority' =>'1000',
        'url'   => route('admin.newsletter'),
    ],
    [
        'title' => 'Настройки',
        'icon' => 'fa fa-cogs',
        'priority' =>'1001',
        'id'=>'main-examples',
    ],
    // Examples
    // [
    //    'title' => 'Content',
    //    'pages' => [
    //
    //        \App\User::class,
    //
    //        // or
    //
    //        (new Page(\App\User::class))
    //            ->setPriority(100)
    //            ->setIcon('fa fa-user')
    //            ->setUrl('users')
    //            ->setAccessLogic(function (Page $page) {
    //                return auth()->user()->isSuperAdmin();
    //            }),
    //
    //        // or
    //
    //        new Page([
    //            'title'    => 'News',
    //            'priority' => 200,
    //            'model'    => \App\News::class
    //        ]),
    //
    //        // or
    //        (new Page(/* ... */))->setPages(function (Page $page) {
    //            $page->addPage([
    //                'title'    => 'Blog',
    //                'priority' => 100,
    //                'model'    => \App\Blog::class
	//		      ));
    //
	//		      $page->addPage(\App\Blog::class);
    //	      }),
    //
    //        // or
    //
    //        [
    //            'title'       => 'News',
    //            'priority'    => 300,
    //            'accessLogic' => function ($page) {
    //                return $page->isActive();
    //		      },
    //            'pages'       => [
    //
    //                // ...
    //
    //            ]
    //        ]
    //    ]
    // ]
];