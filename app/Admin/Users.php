<?php

use App\Models\Users;
use App\Role;
use App\User;
use SleepingOwl\Admin\Form\FormElements;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(\App\Models\Users::class, function (ModelConfiguration $model){


    $model->creating(function (ModelConfiguration $model, \App\Models\Users $company) {

        $phone = Request::input('phone');
        $surname = Request::input('surname');
        $region = Request::input('region');
        $name = Request::input('name');
        $area = Request::input('area');
        $email = Request::input('email');
        $rememberToken = Request::input('_token');
        $password = bcrypt(trim(urldecode(htmlspecialchars((Request::input('password'))))));
        $role = Request::input('role');
        $date = date('Y-m-d h:i:s');

        $users = DB::table('users')->select('email')->get();
        $usersEmail = json_decode(json_encode($users), true);

        foreach ($usersEmail as $userEmail) {
            foreach ($userEmail as $itemEmail => $valueEmail) {
                $arr = $valueEmail;
                if ($email === $valueEmail) {
                    $arr = $valueEmail;
                }
            }
        }
        if ($email === $arr) {
            echo "
             <script >
                alert('Такой емайл уже используется повторите попытку!!');
                history.back();
             </script> ";
            die;
        } else {
            $users = DB::table('users')->insert(
                [
                    'name' => $name,
                    'email' => $email,
                    'area' => $area,
                    'region' => $region,
                    'surname' => $surname,
                    'phone' => $phone,
                    'password' => $password,
                    'remember_token' => $rememberToken,
                    'created_at' => $date,
                    'updated_at' => $date
                ]
            );

            $userRole = Role::where('name', 'User')->first();
            $user = User::where('email', '=', $email)->first();
            $user->roles()->attach($userRole->id);
            header('Location: /admin/users');
        }
        die;

    });



    $model->setTitle('Пользователи');
    $model->onDisplay(function (){

        $display = AdminDisplay::table();

        $display->setHtmlAttribute('class', 'table-bordered table-primary table-hover');

        $display->getApply()->push(function ($query) {
            $query->orderBy('name', 'asc');
        });

        $display->paginate(10);
        $display->with('roles');

        $display->setColumns([
            AdminColumn::text('name', 'Имя'),
            AdminColumn::email('email', 'Email'),
            AdminColumnEditable::text('phone')->setLabel('Телефон'),
            AdminColumn::datetime('created_at')->setLabel('Дата регитрации')->setFormat('d.m.Y'),
            AdminColumn::lists('roles.name', 'Роль'),
        ]);
        $display->getColumns()->getControlColumn();

        return $display;
    });

    $model->onCreate(function($id = null) {

        $formPrimary = AdminForm::form()->addElement(
            AdminFormElement::columns()
                ->addColumn([
                    AdminFormElement::text('name', 'Имя')
                        ->required('Имя должно быть заполненно'),
                    AdminFormElement::text('email', 'Email')
                        ->required('Email должен быть заполненно')
                        ->addValidationRule('email')
                        ->unique(),
                    AdminFormElement::text('surname', 'Фамилия')
                        ->required('Фамилия должна быть заполненно'),
                    AdminFormElement::text('region', 'Область')
                        ->required('Область должна быть заполненно'),
                ], 6)
                ->addColumn([
                    AdminFormElement::text('area', 'Город')
                        ->required('Город должен быть заполненно'),
                    AdminFormElement::text('phone', 'Телефон')
                        ->required('Телефон должен быть заполненно'),
                    AdminFormElement::password('password', 'Пароль')
                        ->required('Поле "Пароль" обязетельно для заполнения')
                        ->addValidationRule('min:6','Пароль должен быть не менее 6 символов.')
                        ->hashWithBcrypt(),
                    AdminFormElement::text('password_confirmation', 'Подтверждение пароля')
                        ->setValueSkipped(true)
                        ->required()
                        ->addValidationRule('same:password', 'Пароли должны совпадать!'),
                    AdminFormElement::multiselect('roles','Роль')
                        ->required('Роль должно быть выбранная')
                        ->setModelForOptions(new Role)
                        ->setDefaultValue('3')
                        ->setLabel('Роль')
                        ->setDisplay('name')
                ], 6)
        );
        $tabs = AdminDisplay::tabbed();

        $tabs->appendTab($formPrimary,  'Создание пользователя');

        return $tabs;

    });


    $model->onEdit(function($id = null) {


        $formPrimary = AdminForm::form()->addElement(
            AdminFormElement::columns()
                ->addColumn([
                    AdminFormElement::text('name', 'Имя')
                        ->required(),
                    AdminFormElement::text('email', 'Email')
                        ->required()
                        ->addValidationRule('email')
                        ->unique(),
                    AdminFormElement::text('surname', 'Фамилия')
                        ->required(),
                    AdminFormElement::text('region', 'Область')->required(),
                    AdminFormElement::text('area', 'Город')->required(),
                    AdminFormElement::text('phone', 'Телефон')->required(),
                ], 6)
        );
        $formHTML = AdminForm::form()->addElement(
            new FormElements([
                AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::multiselect('roles','Роль')
                            ->required('Роль должно быть выбранная')
                            ->setModelForOptions(new Role)
                            ->setLabel('Роль')
                            ->setDisplay('name')

                    ], 6)
            ])
        );
        $formVisual = AdminForm::form()->addElement(
            new FormElements([
                AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::password('password', 'Пароль')
                            ->required('Поле "Пароль" обязетельно для заполнения')
                            ->addValidationRule('min:6','Пароль должен быть не менее 6 символов.')
                            ->hashWithBcrypt(),
                        AdminFormElement::text('password_confirmation', 'Подтверждение пароля')
                            ->setValueSkipped(true)
                            ->required('Поле "Подтверждение пароля" обязетельно для заполнения')
                            ->addValidationRule('same:password', 'Пароли должны совпадать!'),
                    ], 6)
            ])
        );

        $tabs = AdminDisplay::tabbed();

        $tabs->appendTab($formPrimary,  'Основные настройки');

        $tabs->appendTab($formHTML,     'Изменить роль');

        $tabs->appendTab($formVisual,   'Изменить пароль');

        return $tabs;

    });


});