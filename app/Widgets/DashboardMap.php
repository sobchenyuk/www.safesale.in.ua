<?php
/**
 * Created by PhpStorm.
 * User: Андрей
 * Date: 06.11.2017
 * Time: 15:41
 */

namespace App\Widgets;

use SleepingOwl\Admin\Widgets\Widget;

class DashboardMap extends Widget
{

    /**
     * Get content as a string of HTML.
     *
     * @return string
     */
    public function toHtml()
    {
        return view('admin.dashboard.map')->render();
    }

    /**
     * @return string|array
     */
    public function template()
    {
        return \AdminTemplate::getViewPath('dashboard');
    }

    /**
     * @return string
     */
    public function block()
    {
        return 'block.top';
    }
}