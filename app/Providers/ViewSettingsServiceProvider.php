<?php

namespace App\Providers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewSettingsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //Основные настройки
        $mainSettings = DB::table('main_settings')->get();

        foreach ($mainSettings as $item):
            $description = $item->description;
            $mainTitle = $item->title;
            $mainHeader = $item->header;
            $copyright = $item->copyright;
        endforeach;

        //Социальные сети
        $social = DB::table('social')->get();

        //Ссылки на страницы в подвале и не только
        $pagesLink = DB::table('pages')->get();

        //Город
        $towns = DB::table('town')->select('title')->get();

        //Область
        $regions = DB::table('region')->select('title')->get();

        //Районы городов
        $areas = DB::table('area')->select('title')->get();



        View::share([
            'description'=>$description,
            'mainHeader'=>$mainHeader,
            'mainTitle'=>$mainTitle,
            'copyright'=>$copyright,
            'social'=>$social,
            'pagesLink'=>$pagesLink,
            'towns' => $towns ,
            'regions'=> $regions,
            'areas'=> $areas,
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
