<?php namespace App\Models;

use App\Http\Requests\Request;
use App\Role;
use App\User;

class Users extends SiteModel {


    public $table = 'users';

    protected $fillable = [
        'name',
        'email',
        'password',
        'surname',
        'region',
        'area',
        'phone',
        'role'
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_user', 'user_id', 'role_id');
    }

}