<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SiteModel extends Model
{
    //

    protected $fillable = ['role_user', 'user_id', 'role_id', 'role', 'password'];
}
