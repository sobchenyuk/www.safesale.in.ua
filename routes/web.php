<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//login
Route::get('login', function () {
    return view('login');
})->middleware('signed');
//registration
Route::get('/register', function () {
    return view('register');
})->middleware('signed');
//warning
Route::get('warning', function () {
    return view('warning.warning');
})->middleware('signed');

//user panel
Route::group(['middleware' => 'authorized','prefix' => 'personal-account'], function()
{
    //Активные объявления
    Route::any('/', 'PersonaAccount@showHome');

    Route::any('/{id}', 'PersonaAccount@tabView');
});
//captcha
Route::get('/get_captcha/{config?}', function (\Mews\Captcha\Captcha $captcha, $config = 'default') {
    return $captcha->src($config);
});

//Route::match(['get', 'post'], 'user/{id}', 'UserController@showProfile');

//home
Route::get('/', 'ShowHome');

//categories-blog
Route::get('/product-blog', function () {
    return view('product-blog.index');
});
//categories-card
Route::get('/product-card', function () {
    return view('product-card.index');
});
//ad
Route::get('/ad', 'AdsController')
    ->middleware('authorized');

//pages
$pages = DB::table('pages')->get();
foreach ($pages as $page) {
    Route::get($page->alias, 'ShowHome@pages');
}

//Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();



//Route::get('/create', function () {
//
//
//    Schema::table('pages', function(Blueprint $table)
//    {
//        $table->string('alias')->after('id');
//    });
//
//});



//Route::get('/start', function () {
//    if(Entrust::hasRole('Admin') || Entrust::hasRole('Moderator')) {
////        return Redirect::to('/admin');
//        return View('admin::name');
//    }
//});



use App\Permission;
use App\Role;
use App\User;
//Route::get('/start', function() {
//    $users = Role::where('name', 'User')->first();
//
//        $user = User::where('email', '=', 'optimist.biz.ua@gmail.com')->first();
//    $user->roles()->attach($users);
//
//});
//use Illuminate\Support\Facades\DB;
//
//
//Route::get('/start', function() {
//
////    $admin = new Role();
////    $admin->name = 'Admin';
////    $admin->display_name = 'Администратор сайта'; // optional
////    $admin->description  = 'Пользователь является администратором сайта'; // optional
////    $admin->save();
//
////    $user = User::where('email', '=', 'ssobchenyuk@mail.ru')->first();
//
////    echo '<pre>';
////    var_dump($user);
////    echo '<pre>';
//
////    $user->attachRole($admin);
//
//
//    $moderatorRole = DB::table('roles')->where('name', '=', 'UserBanned')->pluck('id');
////
//    $user = User::where('email', '=', 'optimist.biz.ua@gmail.com')->first();
//    $user->roles()->attach($moderatorRole);
//
//
////    $adminRole = DB::table('roles')->where('name', '=', 'Admin')->pluck('id');
//
////    $moderator = Role::where('name', 'Moderator')->first();
//
////    $assigningRoles = Permission::where('name', 'assigning-roles')->first();
//
//
////    var_dump($assigningRoles);
//
//
////    $assigningRoles = new Permission();
////    $assigningRoles->name         = 'assigning-roles';
////    $assigningRoles->display_name = 'Назначение ролей'; // optional
////    $assigningRoles->description  = 'Назначение ролей и доступ к настройкам страницы'; // optional
////    $assigningRoles->save();
////
////
////    $creatingEditingPage = new Permission();
////    $creatingEditingPage->name         = 'creating-editing-page';
////    $creatingEditingPage->display_name = 'Редактирование создание страницы'; // optional
////    $creatingEditingPage->description  = 'Редактирование, создание и добавление страницы'; // optional
////    $creatingEditingPage->save();
////
////
////
////    $creatingDeletingPublications = new Permission();
////    $creatingDeletingPublications->name         = 'creating-deleting-publications';
////    $creatingDeletingPublications->display_name = 'Создание и удалений публикаций'; // optional
////    $creatingDeletingPublications->description  = 'Создание и удалений публикаций на сайте'; // optional
////    $creatingDeletingPublications->save();
////
////    $sendingMessages = new Permission();
////    $sendingMessages->name         = 'sending-messages';
////    $sendingMessages->display_name = 'Рассылка сообщений'; // optional
////    $sendingMessages->description  = 'Рассылка сообщений от имени сайта'; // optional
////    $sendingMessages->save();
////
////
////    $moderateComments = new Permission();
////    $moderateComments->name         = 'moderate-comments';
////    $moderateComments->display_name = 'Модерация комментариев'; // optional
////    $moderateComments->description  = 'Модерация комментариев на странице'; // optional
////    $moderateComments->save();
////
////    $blockingUsers = new Permission();
////    $blockingUsers->name         = 'blocking-users';
////    $blockingUsers->display_name = 'Блокировка пользователей'; // optional
////    $blockingUsers->description  = 'Блокировка пользователей на странице'; // optional
////    $blockingUsers->save();
////
////    $viewStatistics = new Permission();
////    $viewStatistics->name         = 'view-statistics';
////    $viewStatistics->display_name = 'Просмотр статистики'; // optional
////    $viewStatistics->description  = 'Просмотр статистики на странице'; // optional
////    $viewStatistics->save();
////
////    $settingPreferences = new Permission();
////    $settingPreferences->name         = 'setting-preferences';
////    $settingPreferences->display_name = 'Задание настроек'; // optional
////    $settingPreferences->description  = 'Задание стандартных настроек для сайта'; // optional
////    $settingPreferences->save();
////
////    $changeLogoHeader = new Permission();
////    $changeLogoHeader->name         = 'change-logo-header';
////    $changeLogoHeader->display_name = 'Изменение заголовка и логотипа'; // optional
////    $changeLogoHeader->description  = 'Изменение заголовка сайта и логотипа'; // optional
////    $changeLogoHeader->save();
////
////    $editContacts = new Permission();
////    $editContacts->name         = 'edit-contacts';
////    $editContacts->display_name = 'Изменение контактов'; // optional
////    $editContacts->description  = 'Изменение контактной информации'; // optional
////    $editContacts->save();
////
////    $settingPhotos = new Permission();
////    $settingPhotos->name         = 'setting-photos';
////    $settingPhotos->display_name = 'Настройка фотографий'; // optional
////    $settingPhotos->description  = 'Настройка отображения фотографий'; // optional
////    $settingPhotos->save();
////
////
////    $addingCategories = new Permission();
////    $addingCategories->name         = 'adding-categories';
////    $addingCategories->display_name = 'Добавление категорий'; // optional
////    $addingCategories->description  = 'Добавление всех возможных категорий'; // optional
////    $addingCategories->save();
////
////    $uploadingFiles = new Permission();
////    $uploadingFiles->name         = 'uploading-files';
////    $uploadingFiles->display_name = 'Загрузка файлов'; // optional
////    $uploadingFiles->description  = 'Загружать файлы на сайт'; // optional
////    $uploadingFiles->save();
////
////    $editAds = new Permission();
////    $editAds->name         = 'edit-ads';
////    $editAds->display_name = 'Изменять обьявления'; // optional
////    $editAds->description  = 'Изменять обьявления на сайте '; // optional
////    $editAds->save();
////
////
////
////    $changeRates = new Permission();
////    $changeRates->name         = 'change-rates';
////    $changeRates->display_name = 'Менять ставки'; // optional
////    $changeRates->description  = 'Менять ставки'; // optional
////    $changeRates->save();
////
////    $addExtensions = new Permission();
////    $addExtensions->name         = 'add-extensions';
////    $addExtensions->display_name = 'Добовлять расширения'; // optional
////    $addExtensions->description  = 'Добовлять расширения'; // optional
////    $addExtensions->save();
////
////    $blockAds = new Permission();
////    $blockAds->name         = 'block-ads';
////    $blockAds->display_name = 'Блокировать обьявления'; // optional
////    $blockAds->description  = 'Блокировать обьявления'; // optional
////    $blockAds->save();
//
////    $moderator = Role::where('name', 'Moderator')->first();
////
////    $sendingMessages = Permission::where('name', 'sending-messages')->first();
////    $moderateComments = Permission::where('name', 'moderate-comments')->first();
////    $blockingUsers = Permission::where('name', 'blocking-users')->first();
////    $uploadingFiles = Permission::where('name', 'uploading-files')->first();
////    $editAds = Permission::where('name', 'edit-ads')->first();
////    $blockAds = Permission::where('name', 'block-ads')->first();
////
////    $moderator->perms()->sync([
////        $sendingMessages->id,
////        $moderateComments->id,
////        $blockingUsers ->id,
////        $uploadingFiles ->id,
////        $editAds ->id,
////        $blockAds ->id
////
////    ]);
//
//
//
////    $hasAccess = new Permission();
////    $hasAccess->name         = 'has-access';
////    $hasAccess->display_name = 'Имеет доступ'; // optional
////    $hasAccess->description  = 'Имеет доступ к личному кабинету'; // optional
////    $hasAccess->save();
////
////    $unlimitedUser = new Permission();
////    $unlimitedUser->name         = 'unlimited-user';
////    $unlimitedUser->display_name = 'Пользователь без ограничений'; // optional
////    $unlimitedUser->description  = 'Может пользоваться личным кабинетом без ограничений'; // optional
////    $unlimitedUser->save();
////
////    $phoneNotChange = new Permission();
////    $phoneNotChange->name         = 'phone-not-change';
////    $phoneNotChange->display_name = 'Телефон не меняет'; // optional
////    $phoneNotChange->description  = 'Не может менять номер телефона'; // optional
////    $phoneNotChange->save();
////
////
////
////
////
////
////
////    $noAd = new Permission();
////    $noAd->name         = 'no-ad';
////    $noAd->display_name = 'Нет обьявления'; // optional
////    $noAd->description  = 'Не может создавать обьявления'; // optional
////    $noAd->save();
////
////
////    $withoutBids = new Permission();
////    $withoutBids->name         = 'without-bids';
////    $withoutBids->display_name = 'Без ставок'; // optional
////    $withoutBids->description  = 'Не может учавствовать в ставках'; // optional
////    $withoutBids->save();
////
////
////    $withoutCorrespondence = new Permission();
////    $withoutCorrespondence->name         = 'without-correspondence';
////    $withoutCorrespondence->display_name = 'Без переписки'; // optional
////    $withoutCorrespondence->description  = 'Не может вести переписку с пользователями'; // optional
////    $withoutCorrespondence->save();
////
////    $withoutMessage = new Permission();
////    $withoutMessage->name         = 'without-message';
////    $withoutMessage->display_name = 'Без сообщения'; // optional
////    $withoutMessage->description  = 'Не может получать сообщения'; // optional
////    $withoutMessage->save();
////
////
////
////    $user = Role::where('name', 'User')->first();
////    $userBanned = Role::where('name', 'UserBanned')->first();
////
////
////    $user->perms()->sync([
////        $hasAccess->id,
////        $unlimitedUser->id,
////        $phoneNotChange ->id,
////        $withoutMessage ->id,
////    ]);
////
////    $userBanned->perms()->sync([
////        $hasAccess->id,
////        $noAd->id,
////        $withoutBids->id,
////        $withoutCorrespondence->id,
////    ]);
//
//
//});