@extends('layouts.theme')

@section('title', 'Карточка товара')

@section('body-class', 'product-card')

@section('content')
    <main>

        <div class="container uk-containe">

            <div class="main-container">

                @include('layouts.module.filter')

                <section class="showcase">

                    <article>

                        <div class="article-fluid article-left">

                            <img src="/img/Photo.png" alt="gallery trumbull">
                        </div>

                        <div class="article-fluid article-right">

                            <div class="article-fluid__container">

                                <div class="article-fluid__container--l">

                                    <header>

                                        <h2><b>Продам после ДТП</b></h2>
                                    </header>

                                    <section>

                                        <p class="article-fluid__container--text">
                                            Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
                                            doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
                                            veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim
                                            ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia
                                            consequuntur. Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                                            accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab
                                            illo inventore veritatis et quasi architecto beatae vitae dicta sunt
                                            explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit
                                            aut fugit, sed quia consequunturSed ut perspiciatis unde
                                        </p>

                                        <div class="article-fluid__container--rating">

                                            <ul class="rating-box">

                                                <li class="rating-box__user">

                                                    <img src="/img/man-user.png" alt="man-user">
                                                </li>

                                                <li class="rating-box__userName">

                                                    <b>Василий Пупкин</b>
                                                </li>

                                                <li class="rating-box__stars">
                                                    <ul >
                                                        <li class="rating-box__item">

                                                            <img src="/img/star_true.png" alt="star_true">
                                                        </li>
                                                        <li class="rating-box__item">

                                                            <img src="/img/star_true.png" alt="star_true">
                                                        </li>
                                                        <li class="rating-box__item">

                                                            <img src="/img/star_true.png" alt="star_true">
                                                        </li>
                                                        <li class="rating-box__item">

                                                            <img src="/img/star_true.png" alt="star_true">
                                                        </li>
                                                        <li class="rating-box__item">

                                                            <img src="/img/star_false.png" alt="star_true">
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="article-fluid__container--rating">

                                            <ul class="rating-box">

                                                <li class="rating-box__user">

                                                    <img src="/img/like.png" alt="like">
                                                </li>

                                                <li class="rating-box__userName">

                                                    <b>Рейтинг товара</b>
                                                </li>

                                                <li class="rating-box__stars">
                                                    <ul >
                                                        <li class="rating-box__item">

                                                            <img src="/img/star_true.png" alt="star_true">
                                                        </li>
                                                        <li class="rating-box__item">

                                                            <img src="/img/star_false.png" alt="star_false">
                                                        </li>
                                                        <li class="rating-box__item">

                                                            <img src="/img/star_false.png" alt="star_false">
                                                        </li>
                                                        <li class="rating-box__item">

                                                            <img src="/img/star_false.png" alt="star_false">
                                                        </li>
                                                        <li class="rating-box__item">

                                                            <img src="/img/star_false.png" alt="star_false">
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                    </section>
                                </div>

                                @guest
                                    <div class="article-fluid__container--r" uk-toggle="target: #modal-center">
                                    @else
                                        <div class="article-fluid__container--r">
                                @endguest

                                    <section>

                                        <div class="article-fluid__container--panel">

                                            <div class="views">

                                                <ul>

                                                    <li class="views-img">

                                                        <img src="/img/eye.png" alt="">
                                                    </li>

                                                    <li class="views-text">

                                                        140
                                                    </li>

                                                    <li class="views-date">

                                                        26.03.2017
                                                    </li>
                                                </ul>
                                            </div>

                                            <div class="rate">

                                                <ul>

                                                    <li>

                                                        <div class="buy">

                                                                <span class="rate-text">

                                                                    <span class="rate-text__grey">

                                                                        <b>200</b>
                                                                    </span>
                                                                    <span class="rate-text__with">

                                                                        <b>+2</b>
                                                                    </span>
                                                                </span>

                                                            <span class="buy-color">

                                                                <a href="#">
                                                                  СТАВКА ПОВЫСИТЬ
                                                                </a>
                                                                </span>
                                                        </div>
                                                    </li>
                                                    <li>

                                                        <div class="get">

                                                            <a href="#" class="get-color">
                                                                <b>300 КУПИТЬ</b>

                                                            </a>

                                                            <p>
                                                                12 шт
                                                            </p>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>

                                            <div class="feedback">

                                                <div class="report">
                                                    <img src="/img/alarm-clock.png" alt="">
                                                    1д.21ч
                                                    @guest
                                                        @else
                                                            <span class="report-info">
                                                            <a href="#">
                                                                <img src="/img/info.png" alt="">
                                                            </a>
                                                            <div uk-drop="animation: uk-animation-slide-top-small; duration: 1000"
                                                                 class="uk-drop">
                                                                <div class="uk-card uk-card-body uk-card-default">
                                                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                                                    sed do eiusmod tempor incididunt.
                                                                </div>
                                                            </div>
                                                        </span>
                                                    @endguest
                                                </div>

                                                <div class="call">

                                                    <button >

                                                        ПОЗВОНИТЬ
                                                    </button>
                                                </div>

                                                <div class="message">

                                                    <button>

                                                        СООБЩЕНИЕ
                                                    </button>
                                                </div>

                                                <div class="seller">
                                                    <a href="#" >все товары продавца</a>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>

                            <footer>

                                <ul class="uk-subnav" uk-switcher>
                                    <li>
                                        <a href="#">

                                            <b>
                                                Отзывы
                                                <span class="green">4</span>/<span class="red">1</span>
                                            </b>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#"><b>20 успешных сделок</b></a>
                                    </li>
                                </ul>

                                <ul class="uk-switcher uk-margin">
                                    <li>
                                        <ul class="good">
                                            <li>
                                                <div class="article-fluid__container--rating">

                                                    <ul class="rating-box">

                                                        <li class="rating-box__userName">

                                                            <b>Костя Сахно</b>
                                                        </li>

                                                        <li class="rating-box__stars">
                                                            <ul >
                                                                <li class="rating-box__item">

                                                                    <img src="/img/star_good.png" class="star" alt="star_good">
                                                                </li>
                                                                <li class="rating-box__item">

                                                                    <img src="/img/star_good.png" class="star" alt="star_good">
                                                                </li>
                                                                <li class="rating-box__item">

                                                                    <img src="/img/star_good.png" class="star" alt="star_good">
                                                                </li>
                                                                <li class="rating-box__item">

                                                                    <img src="/img/star_good.png" class="star" alt="star_good">
                                                                </li>
                                                                <li class="rating-box__item">

                                                                    <img src="/img/star_false.png" alt="star_false">
                                                                </li>
                                                            </ul>
                                                        </li>

                                                        <li class="rating-box__date">
                                                            15 вересня 2017
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li class="uk-switcher__text">
                                                <p>
                                                    Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit,
                                                    sed quia consequuntur. Sed ut perspiciatis unde omnis
                                                </p>
                                            </li>
                                        </ul>
                                        <ul class="good">
                                            <li>
                                                <div class="article-fluid__container--rating">

                                                    <ul class="rating-box">

                                                        <li class="rating-box__userName">

                                                            <b>Костя Сахно</b>
                                                        </li>

                                                        <li class="rating-box__stars">
                                                            <ul >
                                                                <li class="rating-box__item">

                                                                    <img src="/img/star_good.png" class="star" alt="star_good">
                                                                </li>
                                                                <li class="rating-box__item">

                                                                    <img src="/img/star_good.png" class="star" alt="star_good">
                                                                </li>
                                                                <li class="rating-box__item">

                                                                    <img src="/img/star_good.png" class="star" alt="star_good">
                                                                </li>
                                                                <li class="rating-box__item">

                                                                    <img src="/img/star_good.png" class="star" alt="star_good">
                                                                </li>
                                                                <li class="rating-box__item">

                                                                    <img src="/img/star_false.png" alt="star_false">
                                                                </li>
                                                            </ul>
                                                        </li>

                                                        <li class="rating-box__date">
                                                            15 вересня 2017
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li class="uk-switcher__text">
                                                <p>
                                                    Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit,
                                                    sed quia consequuntur. Sed ut perspiciatis unde omnis
                                                </p>
                                            </li>
                                        </ul>
                                        <ul class="bad">
                                            <li>
                                                <div class="article-fluid__container--rating">

                                                    <ul class="rating-box">

                                                        <li class="rating-box__userName">

                                                            <b>Костя Сахно</b>
                                                        </li>

                                                        <li class="rating-box__stars">
                                                            <ul >
                                                                <li class="rating-box__item">

                                                                    <img src="/img/star_true.png" class="star" alt="star_true">
                                                                </li>
                                                                <li class="rating-box__item">

                                                                    <img src="/img/star_false.png" alt="star_false">
                                                                </li>
                                                                <li class="rating-box__item">

                                                                    <img src="/img/star_false.png" alt="star_false">
                                                                </li>
                                                                <li class="rating-box__item">

                                                                    <img src="/img/star_false.png" alt="star_false">
                                                                </li>
                                                                <li class="rating-box__item">

                                                                    <img src="/img/star_false.png" alt="star_false">
                                                                </li>
                                                            </ul>
                                                        </li>

                                                        <li class="rating-box__date">
                                                            15 вересня 2017
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li class="uk-switcher__text">
                                                <p>
                                                    Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit,
                                                    sed quia consequuntur. Sed ut perspiciatis unde omnis
                                                </p>
                                            </li>
                                        </ul>
                                        <ul class="good">
                                            <li>
                                                <div class="article-fluid__container--rating">

                                                    <ul class="rating-box">

                                                        <li class="rating-box__userName">

                                                            <b>Костя Сахно</b>
                                                        </li>

                                                        <li class="rating-box__stars">
                                                            <ul >
                                                                <li class="rating-box__item">

                                                                    <img src="/img/star_good.png" class="star" alt="star_good">
                                                                </li>
                                                                <li class="rating-box__item">

                                                                    <img src="/img/star_good.png" class="star" alt="star_good">
                                                                </li>
                                                                <li class="rating-box__item">

                                                                    <img src="/img/star_good.png" class="star" alt="star_good">
                                                                </li>
                                                                <li class="rating-box__item">

                                                                    <img src="/img/star_good.png" class="star" alt="star_good">
                                                                </li>
                                                                <li class="rating-box__item">

                                                                    <img src="/img/star_false.png" alt="star_false">
                                                                </li>
                                                            </ul>
                                                        </li>

                                                        <li class="rating-box__date">
                                                            15 вересня 2017
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li class="uk-switcher__text">
                                                <p>
                                                    Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit,
                                                    sed quia consequuntur. Sed ut perspiciatis unde omnis
                                                </p>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>20 успешных сделок</li>
                                </ul>


                            </footer>
                        </div>
                    </article>
                </section>
            </div>
        </div>
    </main>
@endsection


