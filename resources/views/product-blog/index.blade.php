@extends('layouts.theme')

@section('title', 'Список товаров')

@section('body-class', 'product-blog product-card')

@section('content')
    <main>

        <div class="container uk-containe">

            <div class="main-container">

                @include('layouts.module.filter')

                <section class="showcase">

                    <header class="product-blog__header">


                        <nav class="uk-navbar-container uk-navbar" uk-navbar="">

                            <div class="uk-navbar-left">
                                <ul class="uk-breadcrumb">
                                    <li><a href="#">Item</a></li>
                                    <li><a href="#">Item</a></li>
                                    <li><span href="#">Active</span></li>
                                </ul>
                            </div>

                            <div class="uk-navbar-right">

                                <span class="product-blog__header--hell"></span>

                                <ul class="uk-navbar-nav">

                                    <li class="product-blog__header--true">

                                        <a href="#">К-во ставок</a>
                                    </li>
                                    <li class="product-blog__header--true">

                                        <a href="#">Дата добавления</a>
                                    </li>
                                    <li class="product-blog__header--false">

                                        <a href="#">Цена</a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </header>

                    <section class="showcase-blog showcase-blog__o">

                        <article>

                            <div class="article-fluid article-left">

                                <img src="/img/Photo1.png" class="lazy" alt="photo" width="146" height="146">
                            </div>

                            <div class="article-fluid article-right">

                                <div class="article-fluid__container">

                                    <div class="article-fluid__container--l">

                                        <div class="article-fluid__l-box">
                                            <h2><b>Продам после ДТП</b></h2>

                                            <span class="article-fluid__date">
                                            <time datetime="2017-03-29">26.03.2017</time>
                                        </span>
                                        </div>

                                        <section>

                                            <p class="article-fluid__container--text">
                                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
                                                doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
                                                veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim
                                                ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia
                                                consequuntur
                                            </p>

                                            <div class="article-fluid__container--r-b">


                                                <div class="container--r">

                                                    <div class="article-fluid__container--rating">

                                                        <ul class="rating-box">

                                                            <li class="rating-box__user">

                                                                <img src="/img/man-user.png" alt="man-user">
                                                            </li>

                                                            <li class="rating-box__userName">

                                                                <b>Василий Пупкин</b>
                                                            </li>

                                                            <li class="rating-box__stars">
                                                                <ul>
                                                                    <li class="rating-box__item">

                                                                        <img src="/img/star_true.png" alt="star_true">
                                                                    </li>
                                                                    <li class="rating-box__item">

                                                                        <img src="/img/star_true.png" alt="star_true">
                                                                    </li>
                                                                    <li class="rating-box__item">

                                                                        <img src="/img/star_true.png" alt="star_true">
                                                                    </li>
                                                                    <li class="rating-box__item">

                                                                        <img src="/img/star_true.png" alt="star_true">
                                                                    </li>
                                                                    <li class="rating-box__item">

                                                                        <img src="/img/star_false.png" alt="star_true">
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>

                                                    <div class="article-fluid__container--rating">

                                                        <ul class="rating-box">

                                                            <li class="rating-box__user">

                                                                <img src="/img/like.png" alt="like">
                                                            </li>

                                                            <li class="rating-box__userName">

                                                                <b>Рейтинг товара</b>
                                                            </li>

                                                            <li class="rating-box__stars">
                                                                <ul>
                                                                    <li class="rating-box__item">

                                                                        <img src="/img/star_true.png" alt="star_true">
                                                                    </li>
                                                                    <li class="rating-box__item">

                                                                        <img src="/img/star_false.png" alt="star_false">
                                                                    </li>
                                                                    <li class="rating-box__item">

                                                                        <img src="/img/star_false.png" alt="star_false">
                                                                    </li>
                                                                    <li class="rating-box__item">

                                                                        <img src="/img/star_false.png" alt="star_false">
                                                                    </li>
                                                                    <li class="rating-box__item">

                                                                        <img src="/img/star_false.png" alt="star_false">
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>

                                                <button>

                                                    ОТЗЫВЫ
                                                    <span class="green">4</span>/<span class="red">1</span>
                                                </button>
                                            </div>
                                        </section>
                                    </div>

                                    <span class="article-fluid__hell">

                                </span>

                                    @guest
                                    <div class="article-fluid__container--r" uk-toggle="target: #modal-center">
                                        @else
                                            <div class="article-fluid__container--r">
                                    @endguest

                                        <section>

                                            <div class="article-fluid__container--panel">

                                                <div class="rate">

                                                    <ul>


                                                        <li>

                                                            <div class="buy">

                                                            <span class="rate-text">

                                                                <span class="rate-text__grey">

                                                                    <b>200</b>
                                                                </span>
                                                                <span class="rate-text__with">

                                                                    <b>+2</b>
                                                                </span>
                                                            </span>

                                                                <span class="buy-color">

                                                                <a href="#">
                                                                  СТАВКА ПОВЫСИТЬ
                                                                </a>
                                                            </span>
                                                            </div>
                                                        </li>
                                                        <li>

                                                            <div class="get">

                                                                <a href="#" class="get-color">
                                                                    <b>300 КУПИТЬ</b>

                                                                </a>

                                                                <p>
                                                                    12 шт
                                                                </p>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>

                            </div>
                        </article>
                    </section>

                    <section class="showcase-blog showcase-blog__t">

                        <article>

                            <div class="article-fluid article-left">

                                <img src="/img/Photo1.png" class="lazy" alt="photo" width="146" height="146">
                            </div>

                            <div class="article-fluid article-right">

                                <div class="article-fluid__container">

                                    <div class="article-fluid__container--l">

                                        <div class="article-fluid__l-box">
                                            <h2><b>Продам после ДТП</b></h2>

                                            <span class="article-fluid__date">
                                            <time datetime="2017-03-29">26.03.2017</time>
                                        </span>
                                        </div>

                                        <section>

                                            <p class="article-fluid__container--text">
                                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
                                                doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
                                                veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim
                                                ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia
                                                consequuntur
                                            </p>

                                            <div class="article-fluid__container--r-b">


                                                <div class="container--r">

                                                    <div class="article-fluid__container--rating">

                                                        <ul class="rating-box">

                                                            <li class="rating-box__user">

                                                                <img src="/img/man-user.png" alt="man-user">
                                                            </li>

                                                            <li class="rating-box__userName">

                                                                <b>Василий Пупкин</b>
                                                            </li>

                                                            <li class="rating-box__stars">
                                                                <ul>
                                                                    <li class="rating-box__item">

                                                                        <img src="/img/star_true.png" alt="star_true">
                                                                    </li>
                                                                    <li class="rating-box__item">

                                                                        <img src="/img/star_true.png" alt="star_true">
                                                                    </li>
                                                                    <li class="rating-box__item">

                                                                        <img src="/img/star_true.png" alt="star_true">
                                                                    </li>
                                                                    <li class="rating-box__item">

                                                                        <img src="/img/star_true.png" alt="star_true">
                                                                    </li>
                                                                    <li class="rating-box__item">

                                                                        <img src="/img/star_false.png" alt="star_true">
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>

                                                    <div class="article-fluid__container--rating">

                                                        <ul class="rating-box">

                                                            <li class="rating-box__user">

                                                                <img src="/img/like.png" alt="like">
                                                            </li>

                                                            <li class="rating-box__userName">

                                                                <b>Рейтинг товара</b>
                                                            </li>

                                                            <li class="rating-box__stars">
                                                                <ul>
                                                                    <li class="rating-box__item">

                                                                        <img src="/img/star_true.png" alt="star_true">
                                                                    </li>
                                                                    <li class="rating-box__item">

                                                                        <img src="/img/star_false.png" alt="star_false">
                                                                    </li>
                                                                    <li class="rating-box__item">

                                                                        <img src="/img/star_false.png" alt="star_false">
                                                                    </li>
                                                                    <li class="rating-box__item">

                                                                        <img src="/img/star_false.png" alt="star_false">
                                                                    </li>
                                                                    <li class="rating-box__item">

                                                                        <img src="/img/star_false.png" alt="star_false">
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>

                                                <button>

                                                    ОТЗЫВЫ
                                                    <span class="green">4</span>/<span class="red">1</span>
                                                </button>
                                            </div>
                                        </section>
                                    </div>

                                    <span class="article-fluid__hell">

                                </span>

                                    @guest
                                        <div class="article-fluid__container--r" uk-toggle="target: #modal-center">
                                        @else
                                            <div class="article-fluid__container--r">
                                    @endguest

                                        <section>

                                            <div class="article-fluid__container--panel">

                                                <div class="rate">

                                                    <ul>

                                                        <li>

                                                            <div class="price">
                                                                <b>1700</b>
                                                                <span>ГРН</span>
                                                            </div>
                                                        </li>
                                                        <li>

                                                            <div class="get">

                                                                <a href="#" class="get-color">
                                                                    <b>КУПИТЬ</b>

                                                                </a>

                                                                <p>
                                                                    12 шт
                                                                </p>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>

                            </div>
                        </article>
                    </section>

                    <section class="showcase-blog showcase-blog__f">

                        <article>

                            <div class="article-fluid article-left">

                                <img src="/img/Photo1.png" class="lazy" alt="photo" width="146" height="146">
                            </div>

                            <div class="article-fluid article-right">

                                <div class="article-fluid__container">

                                    <div class="article-fluid__container--l">

                                        <div class="article-fluid__l-box">
                                            <h2><b>Продам после ДТП</b></h2>

                                            <span class="article-fluid__date">
                                            <time datetime="2017-03-29">26.03.2017</time>
                                        </span>
                                        </div>

                                        <section>

                                            <p class="article-fluid__container--text">
                                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                                                accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab
                                                illo inventore veritatis et quasi architecto beatae vitae dicta sunt
                                                explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut
                                                odit aut fugit, sed quia consequunturSed utperspiciatis unde omnis iste
                                                natus error sit voluptatem accusantium doloremque laudantium, voluptatem
                                                quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur
                                            </p>

                                            <div class="article-fluid__container--r-b">


                                                <div class="container--r">

                                                    <div class="article-fluid__container--rating">

                                                        <ul class="rating-box">

                                                            <li class="rating-box__user">

                                                                <img src="/img/man-user.png" alt="man-user">
                                                            </li>

                                                            <li class="rating-box__userName">

                                                                <b>Василий Пупкин</b>
                                                            </li>

                                                            <li class="rating-box__stars">
                                                                <ul>
                                                                    <li class="rating-box__item">

                                                                        <img src="/img/star_true.png" alt="star_true">
                                                                    </li>
                                                                    <li class="rating-box__item">

                                                                        <img src="/img/star_true.png" alt="star_true">
                                                                    </li>
                                                                    <li class="rating-box__item">

                                                                        <img src="/img/star_true.png" alt="star_true">
                                                                    </li>
                                                                    <li class="rating-box__item">

                                                                        <img src="/img/star_true.png" alt="star_true">
                                                                    </li>
                                                                    <li class="rating-box__item">

                                                                        <img src="/img/star_false.png" alt="star_true">
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>

                                                    <div class="article-fluid__container--rating">

                                                        <ul class="rating-box">

                                                            <li class="rating-box__user">

                                                                <img src="/img/like.png" alt="like">
                                                            </li>

                                                            <li class="rating-box__userName">

                                                                <b>Рейтинг товара</b>
                                                            </li>

                                                            <li class="rating-box__stars">
                                                                <ul>
                                                                    <li class="rating-box__item">

                                                                        <img src="/img/star_true.png" alt="star_true">
                                                                    </li>
                                                                    <li class="rating-box__item">

                                                                        <img src="/img/star_false.png" alt="star_false">
                                                                    </li>
                                                                    <li class="rating-box__item">

                                                                        <img src="/img/star_false.png" alt="star_false">
                                                                    </li>
                                                                    <li class="rating-box__item">

                                                                        <img src="/img/star_false.png" alt="star_false">
                                                                    </li>
                                                                    <li class="rating-box__item">

                                                                        <img src="/img/star_false.png" alt="star_false">
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>

                                                <button>

                                                    ОТЗЫВЫ
                                                    <span class="green">4</span>/<span class="red">1</span>
                                                </button>
                                            </div>
                                        </section>
                                    </div>

                                </div>

                            </div>
                        </article>
                    </section>

                    <ul class="uk-pagination uk-flex-center" uk-margin>

                        <li>

                            <a href="#">

                                <span>
                                    <img src="/img/play-button-l.png" class="lazy" alt="">
                                </span>
                            </a>
                        </li>
                        <li class="uk-active"><span>1</span></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li class="uk-disabled"><span>...</span></li>
                        <li><a href="#">12</a></li>
                        <li>

                            <a href="#">

                                <span>
                                    <img src="/img/play-button-r.png" class="lazy" alt="">
                                </span>
                            </a>
                        </li>
                    </ul>
                </section>
            </div>
        </div>
    </main>
@endsection


