@extends('layouts.theme')

@section('title', 'Резистрация пользователя')

@section('body-class', 'registration')

@section('content')

    <section class="registration-form">

        <div class="container">


            <form id="registerForm" class="uk-form-horizontal uk-margin-large" method="POST" action="{{ route('register') }}" >

                <legend class="uk-legend uk-text-center">Регистрация</legend>

                <hr>
                <div class="registration-form__group">

                    {{ csrf_field() }}

                    <div class="uk-margin{{ $errors->has('name') ? ' has-error' : '' }}">

                        <label class="uk-form-label" for="name">Имя</label>

                        <div class="uk-form-controls">
                            <input id="name"
                                   type="text"
                                   class="uk-input"
                                   name="name"
                                   value="{{ old('name') }}"
                                   placeholder="Имя"
                                   autofocus>

                            <div class="alarm-form alarm-true">
                                <img src="/img/correct-symbol.png" alt="correct">
                            </div>
                            <div class="alarm-form alarm-false">
                                <img src="/img/cancel-mark.png" alt="correct">
                            </div>

                            @if ($errors->has('name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="uk-margin{{ $errors->has('surname') ? ' has-error' : '' }}">

                        <label class="uk-form-label" for="surname">Фамилия</label>

                        <div class="uk-form-controls">
                            <input id="surname" type="text" class="uk-input" name="surname" placeholder="Фамилия">

                            <div class="alarm-form alarm-true">
                                <img src="/img/correct-symbol.png" alt="correct">
                            </div>
                            <div class="alarm-form alarm-false">
                                <img src="/img/cancel-mark.png" alt="correct">
                            </div>

                            @if ($errors->has('surname'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('surname') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="uk-margin{{ $errors->has('region') ? ' has-error' : '' }}">

                        <label class="uk-form-label" for="region">Область</label>

                        <div class="uk-form-controls">
                            <input id="region" type="text" class="uk-input" name="region" placeholder="Область">

                            <div class="alarm-form alarm-true">
                                <img src="/img/correct-symbol.png" alt="correct">
                            </div>
                            <div class="alarm-form alarm-false">
                                <img src="/img/cancel-mark.png" alt="correct">
                            </div>

                            @if ($errors->has('region'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('region') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>



                    <div class="uk-margin{{ $errors->has('area') ? ' has-error' : '' }}">

                        <label class="uk-form-label" for="area">Город</label>

                        <div class="uk-form-controls">
                            <input id="area" type="text" class="uk-input" name="area" placeholder="Город">

                            <div class="alarm-form alarm-true">
                                <img src="/img/correct-symbol.png" alt="correct">
                            </div>
                            <div class="alarm-form alarm-false">
                                <img src="/img/cancel-mark.png" alt="correct">
                            </div>

                            @if ($errors->has('area'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('area') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <hr>

                    <div class="uk-margin{{ $errors->has('phone') ? ' has-error' : '' }}">

                        <label class="uk-form-label" for="phone">Номер телефона</label>

                        <div class="uk-form-controls">
                            <input id="phone" type="text" class="uk-input" name="phone" placeholder="Номер телефона">

                            <div class="alarm-form alarm-true">
                                <img src="/img/correct-symbol.png" alt="correct">
                            </div>
                            <div class="alarm-form alarm-false">
                                <img src="/img/cancel-mark.png" alt="correct">
                            </div>

                            @if ($errors->has('area'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="uk-margin{{ $errors->has('email') ? ' has-error' : '' }}">

                        <label class="uk-form-label" for="email">Email</label>

                        <div class="uk-form-controls">
                            <input id="email" type="email"
                                   class="uk-input"
                                   name="email"
                                   value="{{ old('email') }}"
                                   placeholder="Email">

                            <div class="alarm-form alarm-true">
                                <img src="/img/correct-symbol.png" alt="correct">
                            </div>
                            <div class="alarm-form alarm-false">
                                <img src="/img/cancel-mark.png" alt="correct">
                            </div>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="uk-margin{{ $errors->has('password') ? ' has-error' : '' }}">

                        <label class="uk-form-label" for="password">Пароль</label>

                        <div class="uk-form-controls">
                            <input id="password" type="password"
                                   class="uk-input"
                                   name="password"
                                   placeholder="Пароль">

                            <div class="alarm-form alarm-true">
                                <img src="/img/correct-symbol.png" alt="correct">
                            </div>
                            <div class="alarm-form alarm-false">
                                <img src="/img/cancel-mark.png" alt="correct">
                            </div>

                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="uk-margin">

                        <label class="uk-form-label" for="password-confirm">Повторите</label>

                        <div class="uk-form-controls">
                            <input id="password-confirm"
                                   type="password"
                                   class="uk-input"
                                   name="password_confirmation"
                                   placeholder="Повторите"
                                   >

                            <div class="alarm-form alarm-true">
                                <img src="/img/correct-symbol.png" alt="correct">
                            </div>
                            <div class="alarm-form alarm-false">
                                <img src="/img/cancel-mark.png" alt="correct">
                            </div>


                        </div>
                    </div>

                    <hr>

                    <p class="uk-text-left">
                        Введите число с картинки
                    </p>

                    <div uk-grid class="uk-grid">
                        <div class="uk-width-1-2@s uk-first-column">
                            <img src="{{ captcha_src() }}" alt="captcha"
                                 class="captcha-img" data-refresh-config="default">
                        </div>

                        <div id="checkCaptcha" class="uk-width-1-2@s uk-flex captcha {{ $errors->has('captcha') ? 'errorsCaptcha' : ''  }}">
                            <label for="capcha"></label>
                            <input class="uk-input" id="captcha" type="text" name="captcha" style="margin: auto">

                            <div class="alarm-form alarm-true">
                                <img src="/img/correct-symbol.png" alt="correct">
                            </div>
                            <div class="alarm-form alarm-false">
                                <img src="/img/cancel-mark.png" alt="correct">
                            </div>
                        </div>
                    </div>

                    <div class="uk-margin">
                        <a href="#" id="refresh" class="hvr-icon-spin">
                            Обновить картинку
                        </a>
                    </div>

                    <div class="uk-margin uk-grid-small uk-child-width-auto uk-grid uk-checkbox__group">
                        <label><input class="uk-checkbox" type="checkbox" checked="">Уведомления</label>

                        <label><input class="uk-checkbox" type="checkbox" checked="">С
                            <a href="{{url('usloviya_sajta')}}">условиями сайта</a> согласен</label>
                    </div>

                    <p class="uk-text-center registration-form__group--alarm" style="display:none;">
                        <img src="/img/exclamation-mark.png" class="lazy" alt=""> Не верно указан код с картинки
                    </p>

                    <button type="submit">ЗАРЕГЕСТРИРОВАТЬСЯ</button>

                </div>

            </form>
        </div>
    </section>
@endsection


