@extends('layouts.theme')

@section('title', 'Вход')

@section('body-class', 'registration')

@section('background-class', 'b-1')

@section('content')

    <div id="modal-box" class="uk-modal uk-open">
        <div class="uk-modal-dialog uk-modal-body">

            <form method="POST" action="{{ route('login') }}">
                <fieldset class="uk-fieldset">


                    <legend class="uk-legend uk-text-center">Вход</legend>

                    {{ csrf_field() }}

                    <div class="uk-margin {{ $errors->has('email') ? ' has-error' : '' }}">
                        <input id="email"
                               type="text"
                               class="uk-input"
                               name="email" value="{{ old('email') }}"
                               placeholder="E-Mail" autofocus>
                        @if ($errors->has('email'))
                            <script>
                                alert('{{ $errors->first('email') }} повторите попытку входа');
                            </script>
                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                    </div>

                    <div class="uk-margin {{ $errors->has('password') ? ' has-error' : '' }}">
                        <input id="password"
                               type="text"
                               class="uk-input"
                               name="password"
                               placeholder="Пароль" maxlength="20">

                        @if ($errors->has('password'))
                            <script>
                                alert('{{ $errors->first('password') }} повторите попытку входа');
                            </script>
                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                    </div>



                    <div class="uk-margin uk-grid-small uk-child-width-auto uk-grid uk-checkbox__group">

                        <label>
                            <input class="uk-checkbox" type="checkbox" checked="" name="remember" {{ old('remember') ? 'checked' : '' }}>
                            Запомнить меня
                        </label>

                    </div>

                    <p class="modal-close-default__btn uk-flex uk-flex-between" uk-margin>
                        <button type="submit" class="header-item__btn btn--green">ВОЙТИ</button>

                        <a class="uk-button uk-button-text" href="{{url('register')}}">РЕГИСТРАЦИЯ</a>
                    </p>

                    <div class="uk-margin uk-text-center modal-close-default__form-footer">
                        <a href="#">Проблемы со входом?</a>
                        <br>
                        <a class="btn btn-link" href="{{ route('password.request') }}">
                            Забыли пароль?
                        </a>
                    </div>

                </fieldset>
            </form>


        </div>
    </div>
@endsection