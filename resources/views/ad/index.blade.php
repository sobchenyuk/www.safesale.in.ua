@extends('layouts.theme')

@section('title', 'Новое обьявление')

@section('body-class', 'ad registration')

@section('background-class', 'b-1')

@section('content')

    <style>
        .regions-layer {
            border: 1px solid #00be3d;
            position: absolute;
            top: 49px;
            background: #fff;
            font-size: 14px;
            line-height: 18px;
            z-index: 10000;
            width: 685px;
        }
        .regions-layer:before {
            background: url(https://www.olx.ua/static/olxua/packed/img/2f4f010….png) -211px -230px no-repeat;
            width: 19px;
            height: 10px;
            left: 411px;
            top: -10px;
            position: absolute;
            content: ' ';
        }
        .regions-layer .title {
            border-bottom: 1px solid #ccc;
            padding: 14px 0;
            margin: 0 12px;
        }
        .hidden {
            display: none;
        }
        .full {
            width: auto !important;
        }
        .regions-layer a {
            color: #707070;
        }
        .regions-layer .column:first-child {
            border-left: none;
        }
        .regions-layer .column {
            vertical-align: top;
            padding: 10px 0;
            border-left: 1px solid #ccc;
        }
        .regions-layer .column > li {
            width: 33%;
        }

        .regions-layer .column li a {
            padding: 6px 28px 6px 12px;
            display: block;
            position: relative;
            text-decoration: underline;
            color: #d8d8d8;
        }
        .regions-layer .column li a>* {
            color: #707070;
        }
        .clr:after {
            clear: both;
            content: ".";
            display: block;
            height: 0;
            overflow: hidden;
            visibility: hidden;
        }
        .regions-layer .column li.arrow > a > span::after {
            content: "›";
            font-size: 40px;
            right: 8px;
            top: -11px;
            position: absolute;
            color: #00be3d;
        }
        .regions-layer .goBack::before {
            content: "‹";
            vertical-align: top;
            font-size: 40px;
            color: #00be3d;
            position: absolute;
            top: -3px;
            left: 8px;
        }
        .regions-layer #back_region_link {
            padding-left: 20px;
        }
        .regions-layer .column li:hover {
            background: #00be3d;;
        }
        .regions-layer .column li:hover a>*{
            color: #fff;
        }
        .regions-layer .column li:hover a span::after {
            color: #fff;
        }
        .regions-layer .column li:hover a {
            text-decoration: none;
        }
        #shadow {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            z-index: 100;
        }
    </style>

    <!--главный блок-->
    <section class="registration-form ad-form next">

        <div class="container">

            <form class="uk-form-horizontal uk-margin-large">

                <h2 class="uk-legend uk-text-center">Новое объявление</h2>

                <div class="registration-form__group">

                    <div class="uk-margin registration-form__group--f">
                        <label class="uk-form-label" for="form-title">Заголовок</label>
                        <div class="uk-form-controls true">
                            <input class="uk-input" id="form-title" type="text" placeholder="Напишите заголовок">
                            <div class="alarm-form alarm-true">
                                <img src="/img/correct-symbol.png" alt="correct">
                            </div>
                            <div class="alarm-form alarm-false">
                                <img src="/img/cancel-mark.png" alt="correct">
                            </div>
                        </div>
                    </div>


                    <div class="uk-margin" style="position: relative; z-index: 10000;">
                        <label class="uk-form-label" for="form-category">Категория</label>
                        <div class="uk-form-controls true">
                            <input class="uk-input" id="form-category" type="text" placeholder="Выбирите категорию">
                            <div class="alarm-form alarm-true">
                                <img src="/img/correct-symbol.png" alt="correct">
                            </div>
                            <div class="alarm-form alarm-false">
                                <img src="/img/cancel-mark.png" alt="correct">
                            </div>
                        </div>
                        <noindex>
                            <div class="regions-layer" id="regions-layer" style="display: none; ">
                                <div id="regionSearchBox" class="title clr"><a class="regionSelectA1" data-name="" data-id="">
                                        Выберите категорию</a></div>
                                <div id="subregionSearchBox" class="title clr hidden"></div>
                                <div class="table full" id="categoryLinks">

                                    {{ \App\TreePrint::treePrint($tree) }}

                                </div>
                                <div class="table full subregionslinks hidden" id="subCategorylinks"></div>
                            </div>
                        </noindex>
                    </div>

                    <div class="uk-margin uk-grid-small uk-child-width-auto uk-grid uk-checkbox__group">

                        <div class="uk-form-controls">
                            <label><input class="uk-checkbox" type="checkbox" checked>Новый</label>
                            <label><input class="uk-checkbox" type="checkbox" >Б/У</label>
                        </div>
                    </div>

                    <div class="uk-margin uk-margin-group__form uk-flex">
                        <label class="uk-form-label" for="form-price">Цена</label>
                        <div class="uk-form-controls uk-grid-small false uk-grid" uk-grid>

                            <div class="uk-width-1-2 uk-first-column">
                                <input class="uk-input" id="enter-price" type="text" placeholder="Укажите цену">
                            </div>
                            <div class="uk-width-1-6@s">
                                <select class="uk-select" id="form-price">
                                    <option>Грн</option>
                                    <option>Option 02</option>
                                </select>
                            </div>
                            <div class="uk-width-1-4@s uk-flex">
                                <label style="margin: auto;"><input class="uk-checkbox" type="checkbox">Торг</label>
                            </div>
                            <div class="alarm-form alarm-true">
                                <img src="/img/correct-symbol.png" alt="correct">
                            </div>
                            <div class="alarm-form alarm-false">
                                <img src="/img/cancel-mark.png" alt="correct">
                            </div>
                        </div>
                    </div>

                    <div class="uk-margin uk-margin-group__form form-actual registration-form__group--f">
                        <label class="uk-form-label special" for="form-actual">Объявление актуально</label>
                        <div class="uk-form-controls uk-grid" uk-grid>

                            <div class="uk-width-1-6 uk-first-column">
                                <input class="uk-input" id="form-actual" type="text" placeholder="7-30">
                            </div>
                            <div class="uk-width-1-6@s uk-flex" style="padding-left: 20px;">
                                <p style="margin: auto;">Дней</p>
                            </div>
                            <div class="alarm-form alarm-true">
                                <img src="/img/correct-symbol.png" alt="correct">
                            </div>
                            <div class="alarm-form alarm-false">
                                <img src="/img/cancel-mark.png" alt="correct">
                            </div>
                        </div>
                    </div>


                    <!--group input-->

                    <div class="uk-margin registration-form__group--f registration-form__group--box">

                        <div class="uk-grid-small uk-grid" uk-grid>

                            <div class="uk-width-1-3 uk-first-column">

                            </div>

                            <div class="uk-width-1-3 group-box-t">
                                <label style="margin: auto;">
                                    <input class="uk-checkbox" type="checkbox" checked="">
                                    Аукцион
                                </label>
                            </div>

                            <div class="uk-width-1-3">
                                <p>
                                </p>
                            </div>



                            <div class="uk-width-1-3 uk-grid-margin uk-first-column">
                                <label class="uk-form-label special" for="form-min">Минимальная Цена</label>
                            </div>

                            <div class="uk-width-1-3 group-box-t uk-grid-margin">
                                <input class="uk-input" id="form-min" type="text" placeholder="Укажите мин. цену">
                            </div>

                            <div class="uk-width-1-3 uk-grid-margin">
                                <p>
                                    Цена с которой начинаються торги
                                </p>
                            </div>


                            <div class="uk-width-1-3 uk-grid-margin uk-first-column">
                                <label class="uk-form-label special" for="form-money">Шаг ставки</label>
                            </div>

                            <div class="uk-width-1-3 group-box-t uk-grid-margin">
                                <input class="uk-input" id="form-money" type="text" placeholder="2 грн">
                            </div>

                            <div class="uk-width-1-3 uk-grid-margin">
                                <p>
                                    Сумма для повышения ставки
                                </p>
                            </div>


                            <div class="uk-width-1-3 uk-grid-margin uk-first-column">
                                <label class="uk-form-label special" for="form-and-from">Мин. Цена продажи</label>
                            </div>

                            <div class="uk-width-1-3 group-box-t uk-grid-margin">
                                <input class="uk-input" id="form-and-from" type="text" placeholder="От 1 до 1000 ">
                            </div>

                            <div class="uk-width-1-3 uk-grid-margin">
                                <p>
                                    Цена за которую продавец готов продать товар,
                                    на аукционе,
                                    она не публекуеться.
                                </p>
                            </div>

                        </div>

                        <div class="group-body">

                        </div>
                    </div>


                    <div class="uk-margin registration-form__group--f">
                        <label class="uk-form-label special" for="form-num">Количество товара</label>
                        <div class="uk-form-controls true">
                            <input class="uk-input" id="form-num" type="text" placeholder="Введите к-во товара">
                            <div class="alarm-form alarm-true">
                                <img src="/img/correct-symbol.png" alt="correct">
                            </div>
                            <div class="alarm-form alarm-false">
                                <img src="/img/cancel-mark.png" alt="correct">
                            </div>
                        </div>
                    </div>

                    <!--textarea-->
                    <div class="uk-margin registration-form__group--f">

                        <label class="uk-form-label" for="form-textarea">Описание</label>

                        <div class="uk-form-controls controls-textarea">

                            <textarea class="uk-textarea" rows="5" id="form-textarea" placeholder="Напишите описание товара"></textarea>

                            <span class="uk-textarea__text">
                                    Мин. 150 символов
                                </span>
                        </div>
                    </div>
                    <!--textarea  --------------------------------------->


                    <!--upload-->
                    <div class="uk-margin registration-form__group--f">

                        <label class="uk-form-label text-insert" for="form-check-the-product">Фото <span>(0-15)</span></label>

                        <div class="uk-form-controls">

                            <div uk-form-custom>

                                <input type="file">
                                <button class="button-upload" type="button" tabindex="-1">
                                    <img src="/img/upload.png" class="lazy" alt="upload">
                                </button>
                            </div>
                        </div>
                    </div>
                    <!--upload ------------------------------------------>

                    <div class="uk-margin registration-form__group--f">
                        <label class="uk-form-label" for="form-location">Укажите область</label>
                        <div class="uk-form-controls true">
                            <select class="uk-select" id="form-location">
                                <option value="false">Выбирете область</option>
                                @foreach ($regions as $region)
                                    <option>{{ $region->title }}</option>
                                @endforeach
                            </select>
                            <div class="alarm-form alarm-true">
                                <img src="/img/correct-symbol.png" alt="correct">
                            </div>
                            <div class="alarm-form alarm-false">
                                <img src="/img/cancel-mark.png" alt="correct">
                            </div>
                        </div>
                    </div>

                    <div class="uk-margin registration-form__group--f">
                        <label class="uk-form-label" for="form-location">Укажите город</label>
                        <div class="uk-form-controls true">
                            <select class="uk-select" id="form-location">
                                <option value="false">Выбирете город</option>
                                @foreach ($towns as $town)
                                    <option>{{ $town->title }}</option>
                                @endforeach
                            </select>
                            <div class="alarm-form alarm-true">
                                <img src="/img/correct-symbol.png" alt="correct">
                            </div>
                            <div class="alarm-form alarm-false">
                                <img src="/img/cancel-mark.png" alt="correct">
                            </div>
                        </div>
                    </div>


                    <div class="uk-margin registration-form__group--f">
                        <label class="uk-form-label" for="form-location">Укажите район</label>
                        <div class="uk-form-controls true">
                            <select class="uk-select" id="form-location">
                                <option value="false">Выбирете район</option>
                                @foreach ($areas as $area)
                                    <option>{{ $area->title }}</option>
                                @endforeach
                            </select>
                            <div class="alarm-form alarm-true">
                                <img src="/img/correct-symbol.png" alt="correct">
                            </div>
                            <div class="alarm-form alarm-false">
                                <img src="/img/cancel-mark.png" alt="correct">
                            </div>
                        </div>
                    </div>

                    <div class="uk-margin registration-form__group--f">
                        <label class="uk-form-label special" for="form-check-the-product">Регистрация товара</label>
                        <div class="uk-form-controls true">
                            <select class="uk-select" id="form-check-the-product">
                                <option>Бесплатная</option>
                                <option>Option 02</option>
                            </select>
                            <div class="alarm-form alarm-true">
                                <img src="/img/correct-symbol.png" alt="correct">
                            </div>
                            <div class="alarm-form alarm-false">
                                <img src="/img/cancel-mark.png" alt="correct">
                            </div>
                        </div>
                    </div>

                    <div class="uk-flex uk-flex-center">

                        <div>
                            <button disabled class="button-disabled">НАЗАД</button>
                        </div>

                        <div>
                            <button>ДАЛЕЕ</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>


    <div id="shadow" style="display: none"></div>



    <script>
        (function () {
            window.addEventListener('load', list);

            function list(e) {

                let regionsLayer = document.querySelector("#regions-layer");
                let formCategory = document.querySelector("#form-category");
                let category = document.querySelector("#categoryLinks");
                let categoryLinksUl = document.querySelector("#categoryLinks > ul");
                let categoryLinks = document.querySelectorAll("#categoryLinks a");
                let categoryUl = document.querySelectorAll("#categoryLinks ul");
                let rez = categoryUl.length;
                let rezA = categoryLinks.length;
                let regionSearchBox = document.querySelector('#regionSearchBox a');

                function closeShadow() {
                    shadow.addEventListener('click', function (e) {

                        shadow.style.display = "none";
                        regionsLayer.style.display = "none";
                    });
                }

                closeShadow();

                formCategory.addEventListener('click', function (e) {

                    shadow.style.display = "block";
                    regionsLayer.style.display = "block";
                });

                for (let i = 0; i < rez; i++) {
                    categoryUl[i].style.display = "none";
                }

                categoryLinksUl.classList.add("column", "uk-flex", "uk-flex-wrap");
                categoryLinksUl.style.display = "block";

                for (let a = 0; a < rezA; a++) {
                    if (categoryLinks[a].nextSibling !== null) {
                        if (categoryLinks[a].nextSibling.nodeName === "UL") {
                            categoryLinks[a].parentElement.classList.add("arrow");
                        }
                    }


                }

                function close() {

                    let regionsList = document.querySelectorAll("#regions-layer li");
                    let rezB = regionsList.length;
                    let subCategorylinks = document.querySelector("#subCategorylinks");

                    for( let b = 0; b < rezB; b++ ){

                        if(!regionsList[b].classList.contains("arrow")){

                            regionsList[b].addEventListener('click', function (e) {

                                regionsLayer.style.display = "none";

                                formCategory.value = this.innerText;
                                subCategorylinks.innerHTML = "";

                                category.classList.remove("hidden");
                                subCategorylinks.classList.add("hidden");

                                regionSearchBox.classList.remove("goBack");

                                regionSearchBox.setAttribute('id', '');

                                regionSearchBox.innerHTML = "Выберите категорию";
                                subCategorylinks.innerHTML = "";

                                closeShadow();

                            })
                        }
                    }
                }


                function run() {

                    close();
                    let arrow = document.querySelectorAll("#regions-layer .arrow");
                    let rezC = arrow.length;
                    let subCategorylinks = document.querySelector("#subCategorylinks");


                    for ( let c =0; c < rezC; c++ ){

                        arrow[c].addEventListener('click', function (e) {
                            regionSearchBox.setAttribute('id', 'back_region_link');
                            regionSearchBox.classList.add("goBack");
                            regionSearchBox.innerHTML = "Изменить категорию";
                            subCategorylinks.classList.remove("hidden");
                            this.children[1].classList.add("column", "uk-flex", "uk-flex-wrap", "home");
                            category.classList.add("hidden");

                            subCategorylinks.innerHTML = '<ul class="column uk-flex uk-flex-wrap">' +
                                this.children[1].innerHTML +
                                '</ul>';
                            run();
                        });

                    }

                    let backLink = document.querySelector("#back_region_link");

                    if(backLink){
                        backLink.addEventListener('click', function () {

                            close();

                            category.classList.remove("hidden");
                            subCategorylinks.classList.add("hidden");

                            regionSearchBox.classList.remove("goBack");

                            regionSearchBox.setAttribute('id', '');

                            regionSearchBox.innerHTML = "Выберите категорию";
                            subCategorylinks.innerHTML = "";

                        });
                    }

                }

                run();



            }
        })()
    </script>

@endsection


