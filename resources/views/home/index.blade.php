@extends('layouts.theme')

@section('title', 'Главная')

@section('background-class', 'b-1')

@section('content')
    <main>

        <div class="container uk-containe">

            <div class="main-container">


                @include('layouts.module.filter')


                <section class="showcase">

                    <section class="showcase-s promo">

                        <h2>
                            Промо товари
                        </h2>
                        <ul class="showcase-fluid uk-flex  uk-flex-between">

                            <li>

                                <a href="#" class="showcase-fluid__item uk-flex uk-flex-column">
                                      <span class="showcase-fluid__item--img">
                                        <img src="img/1.png" alt="1">
                                        <span class="showcase-fluid__item--color uk-flex uk-flex-column">

                                            <span class="item-color__title">

                                                <b>Перфоратор BOSC...</b>
                                            </span>


                                        </span>
                                    </span>

                                    <span class="showcase-fluid__item--color showcase-fluid__text uk-flex uk-flex-column">
                                            <span class="showcase-fluid__text--border">
                                                <span class="item-color__text item-color__red">

                                                   <span class="showcase-fluid__item--v">

                                                        Ставка
                                                   </span>

                                                   <span class="showcase-fluid__item--n">

                                                        650
                                                   </span>
                                                </span>
                                                <span class="item-color__text item-color__green">

                                                    <span class="showcase-fluid__item--v">

                                                        Выкуп
                                                    </span>

                                                    <span class="showcase-fluid__item--n">

                                                        1200
                                                    </span>
                                                </span>
                                            </span>
                                        </span>                                    </a>


                            </li>
                            <li>

                                <a href="#" class="showcase-fluid__item uk-flex uk-flex-column">

                                      <span class="showcase-fluid__item--img">

                                        <img src="img/2.png" alt="2">

                                        <span class="showcase-fluid__item--color uk-flex uk-flex-column">

                                            <span class="item-color__title">

                                                <b>Перфоратор BOSC...</b>
                                            </span>


                                        </span>
                                    </span>

                                    <span class="showcase-fluid__item--color showcase-fluid__text uk-flex uk-flex-column">
                                            <span class="showcase-fluid__text--border">
                                                <span class="item-color__text item-color__red">

                                                   <span class="showcase-fluid__item--v">

                                                        Ставка
                                                   </span>

                                                   <span class="showcase-fluid__item--n">

                                                        650
                                                   </span>
                                                </span>
                                                <span class="item-color__text item-color__green">

                                                    <span class="showcase-fluid__item--v">

                                                        Выкуп
                                                    </span>

                                                    <span class="showcase-fluid__item--n">

                                                        1200
                                                    </span>
                                                </span>
                                            </span>
                                        </span>
                                </a>


                            </li>
                            <li>

                                <a href="#" class="showcase-fluid__item uk-flex uk-flex-column">

                                      <span class="showcase-fluid__item--img">

                                        <img src="img/3.png" alt="3">

                                        <span class="showcase-fluid__item--color uk-flex uk-flex-column">

                                            <span class="item-color__title">

                                                <b>Перфоратор BOSC...</b>
                                            </span>


                                        </span>
                                    </span>

                                    <span class="showcase-fluid__item--color showcase-fluid__text uk-flex uk-flex-column">
                                            <span class="showcase-fluid__text--border">
                                                <span class="item-color__text item-color__red">

                                                   <span class="showcase-fluid__item--v">

                                                        Ставка
                                                   </span>

                                                   <span class="showcase-fluid__item--n">

                                                        650
                                                   </span>
                                                </span>
                                                <span class="item-color__text item-color__green">

                                                    <span class="showcase-fluid__item--v">

                                                        Выкуп
                                                    </span>

                                                    <span class="showcase-fluid__item--n">

                                                        1200
                                                    </span>
                                                </span>
                                            </span>
                                        </span>                                    </a>


                            </li>
                            <li>

                                <a href="#" class="showcase-fluid__item uk-flex uk-flex-column">

                                      <span class="showcase-fluid__item--img">

                                        <img src="img/4.png" alt="4">

                                        <span class="showcase-fluid__item--color uk-flex uk-flex-column">

                                            <span class="item-color__title">

                                                <b>Перфоратор BOSC...</b>
                                            </span>


                                        </span>
                                    </span>

                                    <span class="showcase-fluid__item--color showcase-fluid__text uk-flex uk-flex-column">
                                            <span class="showcase-fluid__text--border">
                                                <span class="item-color__text item-color__red">

                                                   <span class="showcase-fluid__item--v">

                                                        Ставка
                                                   </span>

                                                   <span class="showcase-fluid__item--n">

                                                        650
                                                   </span>
                                                </span>
                                                <span class="item-color__text item-color__green">

                                                    <span class="showcase-fluid__item--v">

                                                        Выкуп
                                                    </span>

                                                    <span class="showcase-fluid__item--n">

                                                        1200
                                                    </span>
                                                </span>
                                            </span>
                                        </span>                                    </a>


                            </li>
                            <li>

                                <a href="#" class="showcase-fluid__item uk-flex uk-flex-column">

                                      <span class="showcase-fluid__item--img">

                                        <img src="img/5.png" alt="5">

                                        <span class="showcase-fluid__item--color uk-flex uk-flex-column">

                                            <span class="item-color__title">

                                                <b>Перфоратор BOSC...</b>
                                            </span>


                                        </span>
                                    </span>

                                    <span class="showcase-fluid__item--color showcase-fluid__text uk-flex uk-flex-column">
                                            <span class="showcase-fluid__text--border">
                                                <span class="item-color__text item-color__red">

                                                   <span class="showcase-fluid__item--v">

                                                        Ставка
                                                   </span>

                                                   <span class="showcase-fluid__item--n">

                                                        650
                                                   </span>
                                                </span>
                                                <span class="item-color__text item-color__green">

                                                    <span class="showcase-fluid__item--v">

                                                        Выкуп
                                                    </span>

                                                    <span class="showcase-fluid__item--n">

                                                        1200
                                                    </span>
                                                </span>
                                            </span>
                                        </span>                                    </a>


                            </li>

                            <li>

                                <a href="#" class="showcase-fluid__item uk-flex uk-flex-column">
                                      <span class="showcase-fluid__item--img">
                                        <img src="img/1.png" alt="1">
                                        <span class="showcase-fluid__item--color uk-flex uk-flex-column">

                                            <span class="item-color__title">

                                                <b>Перфоратор BOSC...</b>
                                            </span>


                                        </span>
                                    </span>

                                    <span class="showcase-fluid__item--color showcase-fluid__text uk-flex uk-flex-column">
                                            <span class="showcase-fluid__text--border">
                                                <span class="item-color__text item-color__red">

                                                   <span class="showcase-fluid__item--v">

                                                        Ставка
                                                   </span>

                                                   <span class="showcase-fluid__item--n">

                                                        650
                                                   </span>
                                                </span>
                                                <span class="item-color__text item-color__green">

                                                    <span class="showcase-fluid__item--v">

                                                        Выкуп
                                                    </span>

                                                    <span class="showcase-fluid__item--n">

                                                        1200
                                                    </span>
                                                </span>
                                            </span>
                                        </span>                                    </a>


                            </li>
                            <li>

                                <a href="#" class="showcase-fluid__item uk-flex uk-flex-column">

                                      <span class="showcase-fluid__item--img">

                                        <img src="img/2.png" alt="2">

                                        <span class="showcase-fluid__item--color uk-flex uk-flex-column">

                                            <span class="item-color__title">

                                                <b>Перфоратор BOSC...</b>
                                            </span>


                                        </span>
                                    </span>

                                    <span class="showcase-fluid__item--color showcase-fluid__text uk-flex uk-flex-column">
                                            <span class="showcase-fluid__text--border">
                                                <span class="item-color__text item-color__red">

                                                   <span class="showcase-fluid__item--v">

                                                        Ставка
                                                   </span>

                                                   <span class="showcase-fluid__item--n">

                                                        650
                                                   </span>
                                                </span>
                                                <span class="item-color__text item-color__green">

                                                    <span class="showcase-fluid__item--v">

                                                        Выкуп
                                                    </span>

                                                    <span class="showcase-fluid__item--n">

                                                        1200
                                                    </span>
                                                </span>
                                            </span>
                                        </span>
                                </a>


                            </li>
                            <li>

                                <a href="#" class="showcase-fluid__item uk-flex uk-flex-column">

                                      <span class="showcase-fluid__item--img">

                                        <img src="img/3.png" alt="3">

                                        <span class="showcase-fluid__item--color uk-flex uk-flex-column">

                                            <span class="item-color__title">

                                                <b>Перфоратор BOSC...</b>
                                            </span>


                                        </span>
                                    </span>

                                    <span class="showcase-fluid__item--color showcase-fluid__text uk-flex uk-flex-column">
                                            <span class="showcase-fluid__text--border">
                                                <span class="item-color__text item-color__red">

                                                   <span class="showcase-fluid__item--v">

                                                        Ставка
                                                   </span>

                                                   <span class="showcase-fluid__item--n">

                                                        650
                                                   </span>
                                                </span>
                                                <span class="item-color__text item-color__green">

                                                    <span class="showcase-fluid__item--v">

                                                        Выкуп
                                                    </span>

                                                    <span class="showcase-fluid__item--n">

                                                        1200
                                                    </span>
                                                </span>
                                            </span>
                                        </span>                                    </a>


                            </li>
                            <li>

                                <a href="#" class="showcase-fluid__item uk-flex uk-flex-column">

                                      <span class="showcase-fluid__item--img">

                                        <img src="img/4.png" alt="4">

                                        <span class="showcase-fluid__item--color uk-flex uk-flex-column">

                                            <span class="item-color__title">

                                                <b>Перфоратор BOSC...</b>
                                            </span>


                                        </span>
                                    </span>

                                    <span class="showcase-fluid__item--color showcase-fluid__text uk-flex uk-flex-column">
                                            <span class="showcase-fluid__text--border">
                                                <span class="item-color__text item-color__red">

                                                   <span class="showcase-fluid__item--v">

                                                        Ставка
                                                   </span>

                                                   <span class="showcase-fluid__item--n">

                                                        650
                                                   </span>
                                                </span>
                                                <span class="item-color__text item-color__green">

                                                    <span class="showcase-fluid__item--v">

                                                        Выкуп
                                                    </span>

                                                    <span class="showcase-fluid__item--n">

                                                        1200
                                                    </span>
                                                </span>
                                            </span>
                                        </span>                                    </a>


                            </li>
                            <li>

                                <a href="#" class="showcase-fluid__item uk-flex uk-flex-column">

                                      <span class="showcase-fluid__item--img">

                                        <img src="img/5.png" alt="5">

                                        <span class="showcase-fluid__item--color uk-flex uk-flex-column">

                                            <span class="item-color__title">

                                                <b>Перфоратор BOSC...</b>
                                            </span>


                                        </span>
                                    </span>

                                    <span class="showcase-fluid__item--color showcase-fluid__text uk-flex uk-flex-column">
                                            <span class="showcase-fluid__text--border">
                                                <span class="item-color__text item-color__red">

                                                   <span class="showcase-fluid__item--v">

                                                        Ставка
                                                   </span>

                                                   <span class="showcase-fluid__item--n">

                                                        650
                                                   </span>
                                                </span>
                                                <span class="item-color__text item-color__green">

                                                    <span class="showcase-fluid__item--v">

                                                        Выкуп
                                                    </span>

                                                    <span class="showcase-fluid__item--n">

                                                        1200
                                                    </span>
                                                </span>
                                            </span>
                                        </span>                                    </a>


                            </li>

                            <li>

                                <a href="#" class="showcase-fluid__item uk-flex uk-flex-column">
                                      <span class="showcase-fluid__item--img">
                                        <img src="img/1.png" alt="1">
                                        <span class="showcase-fluid__item--color uk-flex uk-flex-column">

                                            <span class="item-color__title">

                                                <b>Перфоратор BOSC...</b>
                                            </span>


                                        </span>
                                    </span>

                                    <span class="showcase-fluid__item--color showcase-fluid__text uk-flex uk-flex-column">
                                            <span class="showcase-fluid__text--border">
                                                <span class="item-color__text item-color__red">

                                                   <span class="showcase-fluid__item--v">

                                                        Ставка
                                                   </span>

                                                   <span class="showcase-fluid__item--n">

                                                        650
                                                   </span>
                                                </span>
                                                <span class="item-color__text item-color__green">

                                                    <span class="showcase-fluid__item--v">

                                                        Выкуп
                                                    </span>

                                                    <span class="showcase-fluid__item--n">

                                                        1200
                                                    </span>
                                                </span>
                                            </span>
                                        </span>                                    </a>


                            </li>
                            <li>

                                <a href="#" class="showcase-fluid__item uk-flex uk-flex-column">

                                      <span class="showcase-fluid__item--img">

                                        <img src="img/2.png" alt="2">

                                        <span class="showcase-fluid__item--color uk-flex uk-flex-column">

                                            <span class="item-color__title">

                                                <b>Перфоратор BOSC...</b>
                                            </span>


                                        </span>
                                    </span>

                                    <span class="showcase-fluid__item--color showcase-fluid__text uk-flex uk-flex-column">
                                            <span class="showcase-fluid__text--border">
                                                <span class="item-color__text item-color__red">

                                                   <span class="showcase-fluid__item--v">

                                                        Ставка
                                                   </span>

                                                   <span class="showcase-fluid__item--n">

                                                        650
                                                   </span>
                                                </span>
                                                <span class="item-color__text item-color__green">

                                                    <span class="showcase-fluid__item--v">

                                                        Выкуп
                                                    </span>

                                                    <span class="showcase-fluid__item--n">

                                                        1200
                                                    </span>
                                                </span>
                                            </span>
                                        </span>
                                </a>


                            </li>
                            <li>

                                <a href="#" class="showcase-fluid__item uk-flex uk-flex-column">

                                      <span class="showcase-fluid__item--img">

                                        <img src="img/3.png" alt="3">

                                        <span class="showcase-fluid__item--color uk-flex uk-flex-column">

                                            <span class="item-color__title">

                                                <b>Перфоратор BOSC...</b>
                                            </span>


                                        </span>
                                    </span>

                                    <span class="showcase-fluid__item--color showcase-fluid__text uk-flex uk-flex-column">
                                            <span class="showcase-fluid__text--border">
                                                <span class="item-color__text item-color__red">

                                                   <span class="showcase-fluid__item--v">

                                                        Ставка
                                                   </span>

                                                   <span class="showcase-fluid__item--n">

                                                        650
                                                   </span>
                                                </span>
                                                <span class="item-color__text item-color__green">

                                                    <span class="showcase-fluid__item--v">

                                                        Выкуп
                                                    </span>

                                                    <span class="showcase-fluid__item--n">

                                                        1200
                                                    </span>
                                                </span>
                                            </span>
                                        </span>                                    </a>


                            </li>
                            <li>

                                <a href="#" class="showcase-fluid__item uk-flex uk-flex-column">

                                      <span class="showcase-fluid__item--img">

                                        <img src="img/4.png" alt="4">

                                        <span class="showcase-fluid__item--color uk-flex uk-flex-column">

                                            <span class="item-color__title">

                                                <b>Перфоратор BOSC...</b>
                                            </span>


                                        </span>
                                    </span>

                                    <span class="showcase-fluid__item--color showcase-fluid__text uk-flex uk-flex-column">
                                            <span class="showcase-fluid__text--border">
                                                <span class="item-color__text item-color__red">

                                                   <span class="showcase-fluid__item--v">

                                                        Ставка
                                                   </span>

                                                   <span class="showcase-fluid__item--n">

                                                        650
                                                   </span>
                                                </span>
                                                <span class="item-color__text item-color__green">

                                                    <span class="showcase-fluid__item--v">

                                                        Выкуп
                                                    </span>

                                                    <span class="showcase-fluid__item--n">

                                                        1200
                                                    </span>
                                                </span>
                                            </span>
                                        </span>                                    </a>


                            </li>
                            <li>

                                <a href="#" class="showcase-fluid__item uk-flex uk-flex-column">

                                      <span class="showcase-fluid__item--img">

                                        <img src="img/5.png" alt="5">

                                        <span class="showcase-fluid__item--color uk-flex uk-flex-column">

                                            <span class="item-color__title">

                                                <b>Перфоратор BOSC...</b>
                                            </span>


                                        </span>
                                    </span>

                                    <span class="showcase-fluid__item--color showcase-fluid__text uk-flex uk-flex-column">
                                            <span class="showcase-fluid__text--border">
                                                <span class="item-color__text item-color__red">

                                                   <span class="showcase-fluid__item--v">

                                                        Ставка
                                                   </span>

                                                   <span class="showcase-fluid__item--n">

                                                        650
                                                   </span>
                                                </span>
                                                <span class="item-color__text item-color__green">

                                                    <span class="showcase-fluid__item--v">

                                                        Выкуп
                                                    </span>

                                                    <span class="showcase-fluid__item--n">

                                                        1200
                                                    </span>
                                                </span>
                                            </span>
                                        </span>                                    </a>


                            </li>
                        </ul>
                    </section>

                    <section class="showcase-s ads">

                        <h2>

                            <b>VIP</b> ОБЪЯВЛЕНИЯ
                        </h2>
                        <ul class="showcase-fluid uk-flex  uk-flex-between">

                            <li>

                                <a href="#" class="showcase-fluid__item uk-flex uk-flex-column">
                                      <span class="showcase-fluid__item--img">
                                        <img src="img/1.png" alt="1">
                                        <span class="showcase-fluid__item--color uk-flex uk-flex-column">

                                            <span class="item-color__title">

                                                <b>Перфоратор BOSC...</b>
                                            </span>


                                        </span>
                                    </span>

                                    <span class="showcase-fluid__item--color showcase-fluid__text uk-flex uk-flex-column">

                                            <span class="showcase-fluid__text--border">

                                               <span class="item-color__text item-color__red">

                                            <span class="showcase-fluid__item--v">

                                            Ставка
                                            </span>

                                            <span class="showcase-fluid__item--n">

                                            650
                                            </span>
                                        </span>
                                        <span class="item-color__text item-color__green">

                                            <span class="showcase-fluid__item--v">

                                                    Выкуп
                                                </span>

                                                <span class="showcase-fluid__item--n">

                                                  1200
                                                </span>
                                        </span>
                                            </span>
                                    </span>
                                </a>
                            </li>
                            <li>

                                <a href="#" class="showcase-fluid__item uk-flex uk-flex-column">

                                      <span class="showcase-fluid__item--img">

                                        <img src="img/2.png" alt="2">

                                        <span class="showcase-fluid__item--color uk-flex uk-flex-column">

                                            <span class="item-color__title">

                                                <b>Перфоратор BOSC...</b>
                                            </span>


                                        </span>
                                    </span>

                                    <span class="showcase-fluid__item--color showcase-fluid__text uk-flex uk-flex-column">

                                            <span class="showcase-fluid__text--border">

                                               <span class="item-color__text item-color__red">

                                            <span class="showcase-fluid__item--v">

                                            Ставка
                                            </span>

                                            <span class="showcase-fluid__item--n">

                                            650
                                            </span>
                                        </span>
                                        <span class="item-color__text item-color__green">

                                            <span class="showcase-fluid__item--v">

                                                    Выкуп
                                                </span>

                                                <span class="showcase-fluid__item--n">

                                                  1200
                                                </span>
                                        </span>
                                            </span>
                                    </span>
                                </a>
                            </li>
                            <li>

                                <a href="#" class="showcase-fluid__item uk-flex uk-flex-column">

                                        <span class="showcase-fluid__item--img">

                                        <img src="img/3.png" alt="3">

                                        <span class="showcase-fluid__item--color uk-flex uk-flex-column">

                                            <span class="item-color__title">

                                                <b>Перфоратор BOSC...</b>
                                            </span>


                                        </span>
                                    </span>

                                    <span class="showcase-fluid__item--color showcase-fluid__text uk-flex uk-flex-column">

                                            <span class="showcase-fluid__text--border">

                                               <span class="item-color__text item-color__red">

                                            <span class="showcase-fluid__item--v">

                                            Ставка
                                            </span>

                                            <span class="showcase-fluid__item--n">

                                            650
                                            </span>
                                        </span>
                                        <span class="item-color__text item-color__green">

                                            <span class="showcase-fluid__item--v">

                                                    Выкуп
                                                </span>

                                                <span class="showcase-fluid__item--n">

                                                  1200
                                                </span>
                                        </span>
                                            </span>
                                    </span>
                                </a>
                            </li>
                            <li>

                                <a href="#" class="showcase-fluid__item uk-flex uk-flex-column">

                                        <span class="showcase-fluid__item--img">

                                        <img src="img/4.png" alt="4">

                                        <span class="showcase-fluid__item--color uk-flex uk-flex-column">

                                            <span class="item-color__title">

                                                <b>Перфоратор BOSC...</b>
                                            </span>


                                        </span>
                                    </span>

                                    <span class="showcase-fluid__item--color showcase-fluid__text uk-flex uk-flex-column">

                                            <span class="showcase-fluid__text--border">

                                               <span class="item-color__text item-color__red">

                                            <span class="showcase-fluid__item--v">

                                            Ставка
                                            </span>

                                            <span class="showcase-fluid__item--n">

                                            650
                                            </span>
                                        </span>
                                        <span class="item-color__text item-color__green">

                                            <span class="showcase-fluid__item--v">

                                                    Выкуп
                                                </span>

                                                <span class="showcase-fluid__item--n">

                                                  1200
                                                </span>
                                        </span>
                                            </span>
                                    </span>
                                </a>
                            </li>
                            <li>

                                <a href="#" class="showcase-fluid__item uk-flex uk-flex-column">

                                        <span class="showcase-fluid__item--img">

                                        <img src="img/5.png" alt="5">

                                        <span class="showcase-fluid__item--color uk-flex uk-flex-column">

                                            <span class="item-color__title">

                                                <b>Перфоратор BOSC...</b>
                                            </span>


                                        </span>
                                    </span>

                                    <span class="showcase-fluid__item--color showcase-fluid__text uk-flex uk-flex-column">

                                            <span class="showcase-fluid__text--border">

                                               <span class="item-color__text item-color__red">

                                            <span class="showcase-fluid__item--v">

                                            Ставка
                                            </span>

                                            <span class="showcase-fluid__item--n">

                                            650
                                            </span>
                                        </span>
                                        <span class="item-color__text item-color__green">

                                            <span class="showcase-fluid__item--v">

                                                    Выкуп
                                                </span>

                                                <span class="showcase-fluid__item--n">

                                                  1200
                                                </span>
                                        </span>
                                            </span>
                                    </span>
                                </a>
                            </li>

                            <li>

                                <a href="#" class="showcase-fluid__item uk-flex uk-flex-column">
                                      <span class="showcase-fluid__item--img">
                                        <img src="img/1.png" alt="1">
                                        <span class="showcase-fluid__item--color uk-flex uk-flex-column">

                                            <span class="item-color__title">

                                                <b>Перфоратор BOSC...</b>
                                            </span>


                                        </span>
                                    </span>

                                    <span class="showcase-fluid__item--color showcase-fluid__text uk-flex uk-flex-column">

                                            <span class="showcase-fluid__text--border">

                                               <span class="item-color__text item-color__red">

                                            <span class="showcase-fluid__item--v">

                                            Ставка
                                            </span>

                                            <span class="showcase-fluid__item--n">

                                            650
                                            </span>
                                        </span>
                                        <span class="item-color__text item-color__green">

                                            <span class="showcase-fluid__item--v">

                                                    Выкуп
                                                </span>

                                                <span class="showcase-fluid__item--n">

                                                  1200
                                                </span>
                                        </span>
                                            </span>
                                    </span>
                                </a>
                            </li>
                            <li>

                                <a href="#" class="showcase-fluid__item uk-flex uk-flex-column">

                                      <span class="showcase-fluid__item--img">

                                        <img src="img/2.png" alt="2">

                                        <span class="showcase-fluid__item--color uk-flex uk-flex-column">

                                            <span class="item-color__title">

                                                <b>Перфоратор BOSC...</b>
                                            </span>


                                        </span>
                                    </span>

                                    <span class="showcase-fluid__item--color showcase-fluid__text uk-flex uk-flex-column">

                                            <span class="showcase-fluid__text--border">

                                               <span class="item-color__text item-color__red">

                                            <span class="showcase-fluid__item--v">

                                            Ставка
                                            </span>

                                            <span class="showcase-fluid__item--n">

                                            650
                                            </span>
                                        </span>
                                        <span class="item-color__text item-color__green">

                                            <span class="showcase-fluid__item--v">

                                                    Выкуп
                                                </span>

                                                <span class="showcase-fluid__item--n">

                                                  1200
                                                </span>
                                        </span>
                                            </span>
                                    </span>
                                </a>
                            </li>
                            <li>

                                <a href="#" class="showcase-fluid__item uk-flex uk-flex-column">

                                        <span class="showcase-fluid__item--img">

                                        <img src="img/3.png" alt="3">

                                        <span class="showcase-fluid__item--color uk-flex uk-flex-column">

                                            <span class="item-color__title">

                                                <b>Перфоратор BOSC...</b>
                                            </span>


                                        </span>
                                    </span>

                                    <span class="showcase-fluid__item--color showcase-fluid__text uk-flex uk-flex-column">

                                            <span class="showcase-fluid__text--border">

                                               <span class="item-color__text item-color__red">

                                            <span class="showcase-fluid__item--v">

                                            Ставка
                                            </span>

                                            <span class="showcase-fluid__item--n">

                                            650
                                            </span>
                                        </span>
                                        <span class="item-color__text item-color__green">

                                            <span class="showcase-fluid__item--v">

                                                    Выкуп
                                                </span>

                                                <span class="showcase-fluid__item--n">

                                                  1200
                                                </span>
                                        </span>
                                            </span>
                                    </span>
                                </a>
                            </li>
                            <li>

                                <a href="#" class="showcase-fluid__item uk-flex uk-flex-column">

                                        <span class="showcase-fluid__item--img">

                                        <img src="img/4.png" alt="4">

                                        <span class="showcase-fluid__item--color uk-flex uk-flex-column">

                                            <span class="item-color__title">

                                                <b>Перфоратор BOSC...</b>
                                            </span>


                                        </span>
                                    </span>

                                    <span class="showcase-fluid__item--color showcase-fluid__text uk-flex uk-flex-column">

                                            <span class="showcase-fluid__text--border">

                                               <span class="item-color__text item-color__red">

                                            <span class="showcase-fluid__item--v">

                                            Ставка
                                            </span>

                                            <span class="showcase-fluid__item--n">

                                            650
                                            </span>
                                        </span>
                                        <span class="item-color__text item-color__green">

                                            <span class="showcase-fluid__item--v">

                                                    Выкуп
                                                </span>

                                                <span class="showcase-fluid__item--n">

                                                  1200
                                                </span>
                                        </span>
                                            </span>
                                    </span>
                                </a>
                            </li>
                            <li>

                                <a href="#" class="showcase-fluid__item uk-flex uk-flex-column">

                                        <span class="showcase-fluid__item--img">

                                        <img src="img/5.png" alt="5">

                                        <span class="showcase-fluid__item--color uk-flex uk-flex-column">

                                            <span class="item-color__title">

                                                <b>Перфоратор BOSC...</b>
                                            </span>


                                        </span>
                                    </span>

                                    <span class="showcase-fluid__item--color showcase-fluid__text uk-flex uk-flex-column">

                                            <span class="showcase-fluid__text--border">

                                               <span class="item-color__text item-color__red">

                                            <span class="showcase-fluid__item--v">

                                            Ставка
                                            </span>

                                            <span class="showcase-fluid__item--n">

                                            650
                                            </span>
                                        </span>
                                        <span class="item-color__text item-color__green">

                                            <span class="showcase-fluid__item--v">

                                                    Выкуп
                                                </span>

                                                <span class="showcase-fluid__item--n">

                                                  1200
                                                </span>
                                        </span>
                                            </span>
                                    </span>
                                </a>
                            </li>

                            <li>

                                <a href="#" class="showcase-fluid__item uk-flex uk-flex-column">
                                      <span class="showcase-fluid__item--img">
                                        <img src="img/1.png" alt="1">
                                        <span class="showcase-fluid__item--color uk-flex uk-flex-column">

                                            <span class="item-color__title">

                                                <b>Перфоратор BOSC...</b>
                                            </span>


                                        </span>
                                    </span>

                                    <span class="showcase-fluid__item--color showcase-fluid__text uk-flex uk-flex-column">

                                            <span class="showcase-fluid__text--border">

                                               <span class="item-color__text item-color__red">

                                            <span class="showcase-fluid__item--v">

                                            Ставка
                                            </span>

                                            <span class="showcase-fluid__item--n">

                                            650
                                            </span>
                                        </span>
                                        <span class="item-color__text item-color__green">

                                            <span class="showcase-fluid__item--v">

                                                    Выкуп
                                                </span>

                                                <span class="showcase-fluid__item--n">

                                                  1200
                                                </span>
                                        </span>
                                            </span>
                                    </span>
                                </a>
                            </li>
                            <li>

                                <a href="#" class="showcase-fluid__item uk-flex uk-flex-column">

                                      <span class="showcase-fluid__item--img">

                                        <img src="img/2.png" alt="2">

                                        <span class="showcase-fluid__item--color uk-flex uk-flex-column">

                                            <span class="item-color__title">

                                                <b>Перфоратор BOSC...</b>
                                            </span>


                                        </span>
                                    </span>

                                    <span class="showcase-fluid__item--color showcase-fluid__text uk-flex uk-flex-column">

                                            <span class="showcase-fluid__text--border">

                                               <span class="item-color__text item-color__red">

                                            <span class="showcase-fluid__item--v">

                                            Ставка
                                            </span>

                                            <span class="showcase-fluid__item--n">

                                            650
                                            </span>
                                        </span>
                                        <span class="item-color__text item-color__green">

                                            <span class="showcase-fluid__item--v">

                                                    Выкуп
                                                </span>

                                                <span class="showcase-fluid__item--n">

                                                  1200
                                                </span>
                                        </span>
                                            </span>
                                    </span>
                                </a>
                            </li>
                            <li>

                                <a href="#" class="showcase-fluid__item uk-flex uk-flex-column">

                                        <span class="showcase-fluid__item--img">

                                        <img src="img/3.png" alt="3">

                                        <span class="showcase-fluid__item--color uk-flex uk-flex-column">

                                            <span class="item-color__title">

                                                <b>Перфоратор BOSC...</b>
                                            </span>


                                        </span>
                                    </span>

                                    <span class="showcase-fluid__item--color showcase-fluid__text uk-flex uk-flex-column">

                                            <span class="showcase-fluid__text--border">

                                               <span class="item-color__text item-color__red">

                                            <span class="showcase-fluid__item--v">

                                            Ставка
                                            </span>

                                            <span class="showcase-fluid__item--n">

                                            650
                                            </span>
                                        </span>
                                        <span class="item-color__text item-color__green">

                                            <span class="showcase-fluid__item--v">

                                                    Выкуп
                                                </span>

                                                <span class="showcase-fluid__item--n">

                                                  1200
                                                </span>
                                        </span>
                                            </span>
                                    </span>
                                </a>
                            </li>
                            <li>

                                <a href="#" class="showcase-fluid__item uk-flex uk-flex-column">

                                        <span class="showcase-fluid__item--img">

                                        <img src="img/4.png" alt="4">

                                        <span class="showcase-fluid__item--color uk-flex uk-flex-column">

                                            <span class="item-color__title">

                                                <b>Перфоратор BOSC...</b>
                                            </span>


                                        </span>
                                    </span>

                                    <span class="showcase-fluid__item--color showcase-fluid__text uk-flex uk-flex-column">

                                            <span class="showcase-fluid__text--border">

                                               <span class="item-color__text item-color__red">

                                            <span class="showcase-fluid__item--v">

                                            Ставка
                                            </span>

                                            <span class="showcase-fluid__item--n">

                                            650
                                            </span>
                                        </span>
                                        <span class="item-color__text item-color__green">

                                            <span class="showcase-fluid__item--v">

                                                    Выкуп
                                                </span>

                                                <span class="showcase-fluid__item--n">

                                                  1200
                                                </span>
                                        </span>
                                            </span>
                                    </span>
                                </a>
                            </li>
                            <li>

                                <a href="#" class="showcase-fluid__item uk-flex uk-flex-column">

                                        <span class="showcase-fluid__item--img">

                                        <img src="img/5.png" alt="5">

                                        <span class="showcase-fluid__item--color uk-flex uk-flex-column">

                                            <span class="item-color__title">

                                                <b>Перфоратор BOSC...</b>
                                            </span>


                                        </span>
                                    </span>

                                    <span class="showcase-fluid__item--color showcase-fluid__text uk-flex uk-flex-column">

                                            <span class="showcase-fluid__text--border">

                                               <span class="item-color__text item-color__red">

                                            <span class="showcase-fluid__item--v">

                                            Ставка
                                            </span>

                                            <span class="showcase-fluid__item--n">

                                            650
                                            </span>
                                        </span>
                                        <span class="item-color__text item-color__green">

                                            <span class="showcase-fluid__item--v">

                                                    Выкуп
                                                </span>

                                                <span class="showcase-fluid__item--n">

                                                  1200
                                                </span>
                                        </span>
                                            </span>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </section>
                </section>
            </div>
        </div>
    </main>
@endsection


