<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <meta name="description" content="{{ $description }}" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $mainTitle }} | @yield('title')</title>

    @include('layouts.partials.css')

    <style>
        .button-box{display:flex;flex-direction:column}.button-box a{margin:auto;text-decoration:underline;font-size:12px;text-transform:uppercase;position:relative;top:4px}header .btn--default{margin:0 20px}header .btn--cart{margin:0}.showcase-fluid{flex-direction:row;flex-wrap:wrap;margin-bottom:0}.showcase-fluid li{margin:auto auto 20px;width:155.56px;height:224px}#modal-box{position:relative;background:0 0;display:block}#modal-box .uk-modal-dialog,#modal-close-default .uk-modal-dialog{border-style:solid;border-width:2px;border-color:#d8dad9;background-color:#f4f3f3;width:340px;border-radius:4px;padding:20px 25px}#modal-box .panel-heading,#modal-close-default .panel-heading{font-size:28px;color:#474646;line-height:1.2}#modal-box .modal-close-default__form-footer a,#modal-close-default .modal-close-default__form-footer a{font-size:12px;color:rgba(61,61,61,.502);text-decoration:underline;line-height:1.2}#modal-box .modal-close-default__btn a,#modal-close-default .modal-close-default__btn a{font-size:15px;color:#949191;font-weight:700;line-height:1.2}#modal-box .modal-close-default__btn,#modal-close-default .modal-close-default__btn{align-items:center}#modal-box .modal-close-default__btn .header-item__btn,#modal-close-default .modal-close-default__btn .header-item__btn{width:138px;height:46px}#modal-box .uk-checkbox,#modal-close-default .uk-checkbox{font-size:14px;color:rgba(61,61,61,.502);line-height:1.2;margin-right:15px}#modal-box .help-block,#modal-close-default .help-block{display:block;color:red;font-size:14px;line-height:1.2;margin-bottom:15px}#modal-box input.error,#modal-close-default input.error{border:1px solid red}#allert{background:#00be3d;color:#fff;padding:5px;border:1px solid;border-radius:2px;margin:0 auto 20px;text-align:center}#allert.error{background:#ff0;color:red}
        .uk-form-controls.false input {
            border: 1px solid red;
        }
        .uk-form-controls.true input {
            border: 1px solid #00be3d;
        }
        span.alert-success {
            color: red;
            font-weight: 600;
        }
        .uk-flex.captcha {
            position: relative;
        }
        .uk-flex.captcha {
            position: relative;
            flex-direction: column;
        }
        .registration-form form span.help-block {
            color: red;
        }
        .captcha.errorsCaptcha #captcha,
        .captcha.errorsCaptcha.false #captcha{
            border: 1px solid red;
        }
        .captcha.errorsCaptcha.true #captcha{
            border: 1px solid #00be3d !important;
        }
        h1 {
            margin: -12px 0 0 auto;
        }
    </style>

</head>
<body class="@yield('body-class')">
    <!--[if lte IE 9]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
    <![endif]-->

    <div class="wrapper" id="app">

        <header>

            <div class="container uk-containe">

                <div uk-grid="" class="uk-grid">

                    <div class="uk-width-1-2 uk-first-column uk-flex uk-flex-top">

                        <div>
                            <a href="{{url('/')}}">
                                <img src="/img/logo_safe_sale.png" class="logo" alt="">
                            </a>
                        </div>
                        <h1>
                            {{ $mainHeader }}
                        </h1>
                    </div>
                    <div class="uk-width-1-2 header-item">

                        <div class="uk-flex uk-flex-right uk-grid uk-grid-stack" uk-grid="">
                            <!-- Навигация -->
                            <nav class="header-item uk-flex uk-first-column">
                                    @guest
                                        <button class="header-item__btn btn--green"
                                                type="button" uk-toggle="target: #modal-center"
                                                {{ Request::path() ==  'register' ? 'disabled' : ''  }}>
                                            ПРОДАТЬ
                                        </button>

                                            @if (Request::path() ==  'register')
                                            @else
                                            <button class="header-item__btn btn--default" uk-toggle="target: #modal-close-default">
                                                <img src="/img/sprite/user.png" alt="">
                                                ВОЙТИ
                                            </button>
                                            <a class="header-item__btn btn--default" href="{{url('register')}}"
                                               style="margin: 0;height: auto; text-decoration: none">
                                                <b>Зарегестрироваться</b>
                                            </a>
                                            @endif
                                        @else
                                                @if(Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Moderator'))

                                            <a href="{{url('admin')}}"
                                               style="padding-top: 7px;">
                                                Админка
                                            </a>
                                            <button class="header-item__btn btn--green" onclick="location.href='{{url('ad')}}'">
                                                ПРОДАТЬ
                                            </button>
                                                    <div class="button-box">
                                                        <button
                                                            id="buttonUserName"
                                                            class="header-item__btn btn--default"
                                                            onclick="location.href='{{url('personal-account')}}'">
                                                        <span>
                                                            Панель пользователя
                                                    </span>
                                                    </button>

                                                        <a href="{{ route('logout') }}"
                                                           onclick="event.preventDefault();
                                                                 document.getElementById('logout-form').submit();"
                                                           class="logout">
                                                            Выход
                                                        </a>

                                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                            {{ csrf_field() }}
                                                        </form>

                                                    </div>



                                                @else

                                            <button class="header-item__btn btn--green" onclick="location.href='{{url('ad')}}'">
                                                ПРОДАТЬ
                                            </button>

                                                    <div class="button-box">

                                                    <button
                                                            id="buttonUserName"
                                                            class="header-item__btn btn--default"
                                                            onclick="location.href='{{url('personal-account')}}'"
                                                            title="Перейти в личный кабинет" uk-tooltip="pos: left">
                                                        <img src="/img/sprite/user.png" alt="">
                                                        <span>
                                                      {{ Auth::user()->name }}
                                                    </span>
                                                    </button>

                                                        <a href="{{ route('logout') }}"
                                                           onclick="event.preventDefault();
                                                                 document.getElementById('logout-form').submit();"
                                                           class="logout">
                                                            Выход
                                                        </a>

                                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                            {{ csrf_field() }}
                                                        </form>
                                                    </div>


                                                    <a class="header-item__btn btn--cart" href="#">
                                                        <img src="/img/sprite/cart.png" alt="">
                                                        &nbsp;&nbsp;
                                                        <b>0,00<span>$</span></b>
                                                    </a>
                                                @endif


                                    @endguest
                            </nav>
                            <!-- Навигация -->
                        </div>
                    </div>
                </div>
            </div>

            <!-- Конец навигации -->
        </header>

        <div class="uk-container uk-container-background">

            <div class="container background-container">
                <div class="background-b @yield('background-class')"></div>
            </div>

            <!--главный блок-->
        @yield('content')
        </div>

    <!--footer-->
        <footer id="footer">

            <div class="f-top">

                <div class="container uk-containe">

                    <div class="uk-child-width-expand@s" uk-grid>

                        <div class="uk-width-1-4">

                            <div class="">

                                <img alt="logo" class="lazy" src="/img/footer_logo_safe_sale.png">
                            </div>
                        </div>
                        <div class="uk-width-3-4">

                            <div class="">

                                <div class="uk-child-width-expand@s " uk-grid>

                                    <div class="uk-width-1-3 f-top__link">

                                        <div class="uk-flex uk-flex-right">

                                            <ul class="uk-list uk-link-text uk-flex uk-flex-column f-top__link--ul">

                                                @foreach ($pagesLink as $page)
                                                    @if($page->position === "Футер левая")
                                                        @if($page->visible === "1")
                                                            <li class="f-top__link--item">
                                                                <a href="{{url($page->alias)}}" class="f-top__item--link">
                                                                    {{ $page->title }}
                                                                </a>
                                                            </li>
                                                        @endif
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="uk-width-1-3 f-top__link">

                                        <div class="uk-flex uk-flex-right">

                                            <ul class="uk-list uk-link-text uk-flex uk-flex-column f-top__link--ul">

                                                @foreach ($pagesLink as $page)
                                                    @if($page->position === "Футер правая")
                                                        @if($page->visible === "1")
                                                            <li class="f-top__link--item">
                                                                <a href="{{url($page->alias)}}" class="f-top__item--link">
                                                                    {{ $page->title }}
                                                                </a>
                                                            </li>
                                                        @endif
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="uk-width-1-3 f-top__link">

                                        <div class="uk-flex uk-flex-right f-top__link--social">

                                            <ul class="uk-list uk-link-text">
                                            
                                            @foreach ($social as $s)
                                                
                                                    <li class="f-top__social--item"
                                                        style="width:29px;height:29px;">

                                                        <a href="{{ $s->link }}"
                                                           class="f-top__social--link">
                                                            <img src="{{ $s->img }}"
                                                                 alt="{{ $s->title }}">
                                                        </a>
                                                    </li>

                                            @endforeach

                                            </ul>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="f-bottom">

                <div class="container uk-containe">

                    <div uk-grid>

                        <div class="uk-width-1-2">

                                    <span class="f-bottom__copyright">
                                            {{ $copyright }}
                                    </span>
                        </div>
                        <div class="uk-width-1-2">

                            <div class="uk-flex uk-flex-right" uk-grid>
                                <div class="">
                                    @foreach ($pagesLink as $page)
                                        @if($page->position === "Позиция копирайта")
                                            @if($page->visible === "1")
                                                <a class="uk-link-reset f-bottom__link"
                                                   href="{{url($page->alias)}}">
                                                    {{ $page->title }}
                                                </a>
                                            @endif
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>

    @include('layouts.partials.scripts')
    @include('layouts.modal')
</body>
</html>
