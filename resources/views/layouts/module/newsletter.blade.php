<form method="POST" action="#" class="panel panel-default">


    <div class="form-elements">
        <div class="panel-body">
            <div class="form-elements">
                <div role="tabpanel" class="nav-tabs-custom ">
                    <div class="tab-content">
                        <div role="tabpanel" id="9084500874f89b773ce41f0d8db6f057" class="tab-pane in active">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-elements">
                                        <div class="form-group form-element-text "><label for="alias"
                                                                                          class="control-label">
                                                Заголовок рассылки

                                                <span class="form-element-required">*</span></label>
                                            <input type="text"
                                                   id="alias"
                                                   name="alias"
                                                   value="obratnaya_svyaz"
                                                   class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-elements"><div class="form-group form-element-textarea "><label for="address" class="control-label">
                                                Тест рассылки
                                                <span class="form-element-required">*</span>
                                            </label> <textarea rows="10" id="address" name="address" class="form-control"></textarea></div></div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="form-buttons panel-footer">
        <div role="group" class="btn-group">

            <button type="submit" name="next_action" value="save_and_continue" class="btn btn-primary"><i
                        class="fa fa-check"></i> Отправить
            </button>
        </div>
    </div>
</form>
