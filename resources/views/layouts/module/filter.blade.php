<!--Сайдбар-->
<aside class="filter">

    <form class="filter-form">
        <fieldset class="uk-fieldset">

            <div class="uk-margin filter-form__fluid filter-form__fluid--s">
                <div class="uk-inline filter-form__fluid--search">
                    <a class="uk-form-icon uk-form-icon-flip" href="#">
                        <img src="/img/search.png" alt="">
                    </a>
                    <input class="uk-input" placeholder="Поиск" type="text">
                </div>
            </div>

            <hr>


            <div class="uk-margin filter-form__fluid">
                <select class="uk-select filter-form__fluid--select">
                    <option value="false">Область</option>
                    @foreach ($regions as $region)
                        <option>{{ $region->title }}</option>
                    @endforeach
                </select>
            </div>
            <div class="uk-margin filter-form__fluid">
                <select class="uk-select filter-form__fluid--select">
                    <option value="false">Город</option>
                    @foreach ($towns as $town)
                        <option>{{ $town->title }}</option>
                    @endforeach
                </select>
            </div>

            <div class="uk-margin filter-form__fluid">
                <select class="uk-select filter-form__fluid--select">
                    <option value="false">Район</option>
                    @foreach ($areas as $area)
                        <option>{{ $area->title }}</option>
                    @endforeach
                </select>
            </div>

            <hr>

            <div class="uk-margin filter-form__fluid">
                <select class="uk-select filter-form__fluid--select">
                    <option>Транспорт</option>
                    <option>Option 01</option>
                    <option>Option 02</option>
                </select>
            </div>

            <div class="uk-margin filter-form__fluid">
                <select class="uk-select filter-form__fluid--select">
                    <option>Авто</option>
                    <option>Option 01</option>
                    <option>Option 02</option>
                </select>
            </div>

            <div class="uk-margin filter-form__fluid">
                <select class="uk-select filter-form__fluid--select">
                    <option>Mercedes</option>
                    <option>Option 01</option>
                    <option>Option 02</option>
                </select>
            </div>

            <div class="uk-margin filter-form__fluid">
                <select class="uk-select filter-form__fluid--select">
                    <option>G55 AMG</option>
                    <option>Option 01</option>
                    <option>Option 02</option>
                </select>
            </div>

            <div class="uk-margin filter-form__fluid">
                <select class="uk-select filter-form__fluid--select">
                    <option>Год 2010 - 2014</option>
                    <option>Option 01</option>
                    <option>Option 02</option>
                </select>
            </div>

            <hr>

            <div class="uk-margin filter-form__fluid--range">

                <div class="filter-form__fluid--range--box">

                                            <span class="range-text">
                                                ЦЕНА
                                            </span>

                    <span class="range-window">

                                                от
                                            </span>

                    <span class="range-window">
                                                до
                                            </span>

                </div>

                <input class="uk-range" type="range" value="0" min="0" max="10" step="0.1">

            </div>

            <hr>

            <div class="uk-margin filter-form__fluid button-f">
                <div class="uk-inline filter-form__fluid--btn">

                    <button>
                        <a class="uk-form-icon">
                            <img src="/img/search.png" alt="">
                        </a>
                        ПОИСК
                    </button>
                </div>
            </div>


        </fieldset>
    </form>
</aside>
<!-- Конец Сайдбар-->