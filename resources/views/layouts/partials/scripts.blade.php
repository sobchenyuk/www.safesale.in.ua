<script>
    ( () => {
        window.addEventListener('load', formChange);

        function formChange() {

            let formChange = document.querySelector('#formChange');
            let registerForm = document.querySelector('#registerForm');

            if(formChange){
                valid(formChange)
            }else if(registerForm) {
                valid(registerForm)
            }
        }

        function valid(obg) {
            let rules = {
                name: [
                    "name",
                    "surname",
                    "region",
                    "area",
                    "phone",
                    "email",
                    "captcha",
                    "password",
                    "password_confirmation"
                ],
                compare: [
                    "password",
                    "password_confirmation"
                ]
            }, i, a, rez = obg.length, resat = rules['name'].length, arr = [];

            obg.addEventListener('submit', function (e) {
                for( i = 0; i < resat; i++ ){
                    for( a = 0; a < rez; a++ ){

                        if(rules['name'][i] === obg[a].name){

                            if(obg[a].name === "password" || obg[a].name === "password_confirmation"){
                                let reg = /[а-яА-ЯёЁ]/g;
                                if (obg[a].value.search(reg) !==  -1) {
                                    obg[a].value = "";
                                    e.preventDefault();
                                    obg[a].parentNode.classList.remove("true");
                                    obg[a].parentNode.classList.add("false");
                                    let span = document.createElement("span");
                                    span.className = "alert-success";
                                    span.innerHTML = "Нельзя использовать русские символы";
                                    obg[a].parentNode.appendChild(span);
                                }
                            }

                            if(obg[a].name === "email" ){

                                let reg = /^((([0-9A-Za-z]{1}[-0-9A-z\.]{1,}[0-9A-Za-z]{1})|([0-9А-Яа-я]{1}[-0-9А-я\.]{1,}[0-9А-Яа-я]{1}))@([-0-9A-Za-z]{1,}\.){1,2}[-A-Za-z]{2,})$/g;
                                if (!reg.test(obg[a].value)) {
                                    if(obg[a].parentNode.lastElementChild.tagName === "SPAN" ) {
                                        obg[a].parentNode.lastElementChild.remove();
                                    }
                                    obg[a].value = "";
                                    e.preventDefault();
                                    obg[a].parentNode.classList.remove("true");
                                    obg[a].parentNode.classList.add("false");
                                    let span = document.createElement("span");
                                    span.className = "alert-success";
                                    span.innerHTML = "Не правильный email";
                                    obg[a].parentNode.appendChild(span);
                                }

                            }

                            if(obg[a].value.length < 1 ){
                                e.preventDefault();

                                obg[a].parentNode.classList.add("false");
                                if(obg[a].parentNode.lastElementChild.tagName !== "SPAN" ){
                                    let span = document.createElement("span");
                                    span.className = "alert-success";
                                    span.innerHTML = "Поле не указано";
                                    obg[a].parentNode.appendChild(span);
                                }
                            }else if ( obg[a].value.length < 5 ){

                                if(obg[a].parentNode.lastElementChild.tagName === "SPAN" ) {
                                    obg[a].parentNode.lastElementChild.remove();
                                }
                                e.preventDefault();
                                obg[a].parentNode.classList.remove("true");
                                obg[a].parentNode.classList.add("false");
                                if(obg[a].parentNode.lastElementChild.tagName !== "SPAN" ){
                                    let span = document.createElement("span");
                                    span.className = "alert-success";
                                    span.innerHTML = "Поле не может быть короче 5 символов";
                                    obg[a].parentNode.appendChild(span);
                                }

                            }
                        }

                        if(rules['compare'][i] === obg[a].name){
                            if( obg[7].value !== obg[8].value ){

                                if (obg[a].parentNode.lastElementChild.tagName === "SPAN") {
                                    obg[a].parentNode.lastElementChild.remove();
                                }
                                e.preventDefault();
                                obg[a].parentNode.classList.remove("true");
                                obg[a].parentNode.classList.add("false");
                                if (obg[a].parentNode.lastElementChild.tagName !== "SPAN") {
                                    let span = document.createElement("span");
                                    span.className = "alert-success";
                                    span.innerHTML = "Пароль не совпадает";
                                    obg[a].parentNode.appendChild(span);
                                }
                            }

                        }

                    }
                }
            });

            for( i = 0; i < resat; i++ ){
                for( a = 0; a < rez; a++ ){
                    if(rules['name'][i] === obg[a].name){
                        handler(obg[a])
                    }
                }
            }

            function handler(obg) {
                obg.addEventListener('input', function (e) {
                    if(this.value.length < 1){
                        this.parentNode.classList.remove("true");
                        this.parentNode.classList.add("false");
                    }else {
                        this.parentNode.classList.remove("false");
                        this.parentNode.classList.add("true");
                        if(this.parentNode.lastElementChild.tagName === "SPAN" ) {
                            this.parentNode.lastElementChild.remove();
                        }

                    }
                });
            }

        }

        window.addEventListener('load', function (e) {
            let registerForm =  document.querySelector('#registerForm');
            if(registerForm){
                let checkCaptcha = document.querySelector('#checkCaptcha');
                let alarm = document.querySelector('.registration-form__group--alarm');
                if(checkCaptcha.classList.contains("errorsCaptcha")){
                    alarm.style.display = "block";
                }
            }
        });

    })();
</script>



<script src="{{ asset('js/main.js') }}"></script>