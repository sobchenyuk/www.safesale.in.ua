@extends('layouts.theme')

@section('title', $title)

@section('body-class', 'personal-account')

@section('content')

    <section class="registration-form">

        <div class="container">

            <div class="personal-container">
                <h2>
                    Личный кабинет
                </h2>
                <div class="personal-container__warning">
                    <div class="img-warning">
                        <img src="/img/exclamation-mark.png" alt="">
                    </div>
                    <p class="warning__text">
                        в случаем не выкупа товара покупателем течение 5 дней, автоматически добавляеться негативный
                        отзыв.в случае не отправление товара покупателю в течение 5 дней, после аукциона, автоматически
                        отправляеться негативный отзыв. Правило сайта и оно не обсуждается !!!
                    </p>
                    <div class="img-warning__mail">
                        <img src="/img/close-envelope.png" alt="">
                    </div>
                </div>
                <div class="personal-container__green"></div>
            </div>

            <div class="personal-account__container">

                <ul class="uk-subnav uk-subnav-pill">
                    <li class="{{ Request::path() ==  'personal-account' ? 'uk-active' : ''  }}">
                        <a href="{{url('personal-account')}}">Активные объявления</a>
                    </li>
                    <li class="{{ Request::path() ==  'personal-account/your-bets' ? 'uk-active' : ''  }}">
                        <a href="{{url('personal-account/your-bets')}}">Ваши ставки</a>
                    </li>
                    <li class="{{ Request::path() ==  'personal-account/messages' ? 'uk-active' : ''  }}">
                        <a href="{{url('personal-account/messages')}}">Сообщения</a>
                    </li>
                    <li class="{{ Request::path() ==  'personal-account/your-purchase' ? 'uk-active' : ''  }}">
                        <a href="{{url('personal-account/your-purchase')}}">Ваши покупки</a>
                    </li>
                    <li class="{{ Request::path() ==  'personal-account/archive' ? 'uk-active' : ''  }}">
                        <a href="{{url('personal-account/archive')}}">Архив</a>
                    </li>
                    <li class="{{ Request::path() ==  'personal-account/settings' ? 'uk-active' : ''  }}">
                        <a href="{{url('personal-account/settings')}}">Настройки</a>
                    </li>
                </ul>

                <ul class="uk-switcher uk-margin">
                    @if (Request::path() ==  'personal-account')
                        <li class="uk-active">
                            @include('personal-account.pages.index')
                        </li>
                    @elseif (Request::path() ==  'personal-account/your-bets')
                        <li class="uk-active">
                            @include('personal-account.pages.your-bets')
                        </li>
                    @elseif (Request::path() ==  'personal-account/messages')
                        <li class="uk-active">
                            @include('personal-account.pages.messages')
                        </li>
                    @elseif (Request::path() ==  'personal-account/your-purchase')
                        <li class="uk-active">
                            @include('personal-account.pages.your-purchase')
                        </li>
                    @elseif (Request::path() ==  'personal-account/archive')
                        <li class="uk-active">
                            @include('personal-account.pages.archive')
                        </li>
                    @elseif (Request::path() ==  'personal-account/settings')
                        <li class="uk-active">
                            @include('personal-account.pages.settings')
                        </li>
                    @else
                        <li></li>
                    @endif
                </ul>

            </div>
        </div>
    </section>
@endsection


