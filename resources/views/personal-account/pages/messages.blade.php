<div class="container-box__switcher">
    <ul>
        <li class="container-box__switcher--true">
            <a>Дата добавления</a>
        </li>
    </ul>
</div>

<ul class="uk-comment-list">
    <li>
        <article class="uk-comment uk-visible-toggle">
            <header class="uk-comment-header uk-position-relative uk-comment-list__first">
                <div class="uk-grid-medium uk-flex-middle uk-grid" uk-grid="">
                    <div class="uk-width-auto uk-first-column">
                        <img class="uk-comment-avatar" src="/img/Photo1.png" width="36" height="36" alt="">
                    </div>
                    <div class="uk-width-expand">
                        <ul class="uk-comment-meta uk-subnav uk-subnav-divider uk-margin-remove-top">
                            <li><a class="uk-comment-meta__title">Продам после ДТП</a></li>
                            <li><a class="uk-comment-meta__number">Осталось <span>9</span>шт.</a></li>
                            <li><a class="uk-comment-meta__price">1200 грн</a></li>
                            <li><a href="#" class="uk-comment-meta__open">ОТКРЫТЬ</a></li>
                        </ul>
                    </div>
                </div>
            </header>
            <div class="uk-comment-body">
                <div uk-grid="" class="uk-comment-body__top uk-grid">
                    <div class="uk-comment-body__grid--left uk-first-column">
                        <ul>
                            <li class="author">Дмитрий Афимов</li>
                            <li class="status__seller">продавец</li>
                            <li class="date">22.10.17 в 18:00</li>
                        </ul>
                    </div>
                    <div class="uk-comment-body__grid--center">
                        <p>
                            Viber стал силой, с которой необходимо считаться, а его популярность
                            среди молодого поколения, постоянно пользующегося смартфонами, выросла
                            неимоверно. В конце концов, смартфоны стали непременным предметом нашего
                            обихода и в ближайшее время никуда не денутся.Viber стал силой, с
                            которой необходимо считаться, а его популярность среди молодого
                            поколения, постоянно пользующегося смартфонами, выросла неимоверно. В
                            конце концов, смартфоны стали непременным предметом нашего обихода и в
                            ближайшее время никуда не денутся.
                        </p>
                    </div>
                    <div class="uk-comment-body__grid--right">

                        <button>
                            ОТВЕТИТЬ
                        </button>

                        <button class="button-orange">
                            УДАЛИТЬ
                        </button>

                        <button class="button-grey">
                            ЗАБЛОКИРОВАТЬ
                        </button>
                    </div>
                </div>
            </div>
        </article>
        <ul>
            <li class="uk-comment__one">
                <article class="uk-comment uk-visible-toggle">
                    <div class="uk-comment-body">
                        <div uk-grid="" class="uk-comment-body__top uk-grid">
                            <div class="uk-comment-body__grid--left uk-first-column">
                                <ul>
                                    <li class="author">Дмитрий Афимов</li>
                                    <li class="status__buyer">покупатель</li>
                                    <li class="date">22.10.17 в 18:00</li>
                                </ul>
                            </div>
                            <div class="uk-comment-body__grid--center">
                                <p>
                                    В конце концов, смартфоны стали непременным предметом
                                    нашего обихода и в ближайшее время никуда не денутся.
                                </p>
                            </div>
                        </div>
                    </div>
                </article>
            </li>
            <li class="uk-comment__two">
                <article class="uk-comment uk-visible-toggle">
                    <div class="uk-comment-body">
                        <div uk-grid="" class="uk-comment-body__top uk-grid">
                            <div class="uk-comment-body__grid--left uk-first-column">
                                <ul>
                                    <li class="author">Дмитрий Афимов</li>
                                    <li class="status__seller">продавец</li>
                                    <li class="date">22.10.17 в 18:00</li>
                                </ul>
                            </div>
                            <div class="uk-comment-body__grid--center">
                                <p>
                                    Которой необходимо считаться, а его популярность среди
                                    молодого поколения, постоянно пользующегося смартфонами,
                                    выросла неимоверно. Ы...
                                </p>
                            </div>
                        </div>
                    </div>
                </article>
            </li>
            <li class="uk-comment__last">
                <a href="#">Читать полностью</a>
            </li>
        </ul>
    </li>
</ul>

<ul class="uk-comment-list">
    <li>
        <article class="uk-comment uk-visible-toggle">
            <header class="uk-comment-header uk-position-relative uk-comment-list__first">
                <div class="uk-grid-medium uk-flex-middle uk-grid" uk-grid="">
                    <div class="uk-width-auto uk-first-column">
                        <img class="uk-comment-avatar" src="/img/Photo1.png" width="36" height="36" alt="">
                    </div>
                    <div class="uk-width-expand">
                        <ul class="uk-comment-meta uk-subnav uk-subnav-divider uk-margin-remove-top">
                            <li><a class="uk-comment-meta__title">Продам после ДТП</a></li>
                            <li><a class="uk-comment-meta__number">Осталось <span>9</span>шт.</a></li>
                            <li><a class="uk-comment-meta__price">1200 грн</a></li>
                            <li><a href="#" class="uk-comment-meta__open">ОТКРЫТЬ</a></li>
                        </ul>
                    </div>
                </div>
            </header>
            <div class="uk-comment-body">
                <div uk-grid="" class="uk-comment-body__top uk-grid">
                    <div class="uk-comment-body__grid--left uk-first-column">
                        <ul>
                            <li class="author">Дмитрий Афимов</li>
                            <li class="status__seller">продавец</li>
                            <li class="date">22.10.17 в 18:00</li>
                        </ul>
                    </div>
                    <div class="uk-comment-body__grid--center">
                        <p>
                            Viber стал силой, с которой необходимо считаться, а его популярность
                            среди молодого поколения, постоянно пользующегося смартфонами, выросла
                            неимоверно. В конце концов, смартфоны стали непременным предметом нашего
                            обихода и в ближайшее время никуда не денутся.Viber стал силой, с
                            которой необходимо считаться, а его популярность среди молодого
                            поколения, постоянно пользующегося смартфонами, выросла неимоверно. В
                            конце концов, смартфоны стали непременным предметом нашего обихода и в
                            ближайшее время никуда не денутся.
                        </p>
                    </div>
                    <div class="uk-comment-body__grid--right">

                        <button>
                            ОТВЕТИТЬ
                        </button>

                        <button class="button-orange">
                            УДАЛИТЬ
                        </button>

                        <button class="button-grey">
                            ЗАБЛОКИРОВАТЬ
                        </button>
                    </div>
                </div>
            </div>
        </article>
        <ul>
            <li class="uk-comment__one">
                <article class="uk-comment uk-visible-toggle">
                    <div class="uk-comment-body">
                        <div uk-grid="" class="uk-comment-body__top uk-grid">
                            <div class="uk-comment-body__grid--left uk-first-column">
                                <ul>
                                    <li class="author">Дмитрий Афимов</li>
                                    <li class="status__buyer">покупатель</li>
                                    <li class="date">22.10.17 в 18:00</li>
                                </ul>
                            </div>
                            <div class="uk-comment-body__grid--center">
                                <p>
                                    В конце концов, смартфоны стали непременным предметом
                                    нашего обихода и в ближайшее время никуда не денутся.
                                </p>
                            </div>
                        </div>
                    </div>
                </article>
            </li>
            <li class="uk-comment__two">
                <article class="uk-comment uk-visible-toggle">
                    <div class="uk-comment-body">
                        <div uk-grid="" class="uk-comment-body__top uk-grid">
                            <div class="uk-comment-body__grid--left uk-first-column">
                                <ul>
                                    <li class="author">Дмитрий Афимов</li>
                                    <li class="status__seller">продавец</li>
                                    <li class="date">22.10.17 в 18:00</li>
                                </ul>
                            </div>
                            <div class="uk-comment-body__grid--center">
                                <p>
                                    Которой необходимо считаться, а его популярность среди
                                    молодого поколения, постоянно пользующегося смартфонами,
                                    выросла неимоверно. Ы...
                                </p>
                            </div>
                        </div>
                    </div>
                </article>
            </li>
            <li class="uk-comment__last">
                <a href="#">Читать полностью</a>
            </li>
        </ul>
    </li>
</ul>