
<form id="formChange" class="uk-form-horizontal uk-margin-large contact-information" method="post">


    <p id="allert" style="display: none"></p>


    {{ csrf_field() }}

    <legend class="uk-legend uk-text-center">изменить контактную информацию</legend>

    <hr>

    <div class="registration-form__group">

        <div class="uk-margin">
            <label class="uk-form-label" for="form-first-name">Имя</label>
            <div class="uk-form-controls">
                <input class="uk-input"
                       id="form-first-name"
                       type="text"
                       name="name"
                       value="{{ $users->name }}">
                <div class="alarm-form alarm-true">
                    <img src="/img/correct-symbol.png" alt="correct">
                </div>
                <div class="alarm-form alarm-false">
                    <img src="/img/cancel-mark.png" alt="correct">
                </div>
            </div>
        </div>

        <div class="uk-margin">
            <label class="uk-form-label" for="form-last-name">Фамилия</label>
            <div class="uk-form-controls">
                <input class="uk-input"
                       id="form-last-name"
                       type="text"
                       name="surname"
                       value="{{ $users->surname }}">
                <div class="alarm-form alarm-true">
                    <img src="/img/correct-symbol.png" alt="correct">
                </div>
                <div class="alarm-form alarm-false">
                    <img src="/img/cancel-mark.png" alt="correct">
                </div>
            </div>
        </div>

        <div class="uk-margin">
            <label class="uk-form-label" for="form-region">Область</label>
            <div class="uk-form-controls">
                <input class="uk-input"
                       id="form-region"
                       type="text"
                       name="region"
                       value="{{ $users->region }}">
                <div class="alarm-form alarm-true">
                    <img src="/img/correct-symbol.png" alt="correct">
                </div>
                <div class="alarm-form alarm-false">
                    <img src="/img/cancel-mark.png" alt="correct">
                </div>
            </div>
        </div>

        <div class="uk-margin">
            <label class="uk-form-label" for="form-district">Город</label>
            <div class="uk-form-controls">
                <input class="uk-input"
                       id="form-district"
                       type="text"
                       name="area"
                       value="{{ $users->area }}">
                <div class="alarm-form alarm-true">
                    <img src="/img/correct-symbol.png" alt="correct">
                </div>
                <div class="alarm-form alarm-false">
                    <img src="/img/cancel-mark.png" alt="correct">
                </div>
            </div>
        </div>

        <hr>

        <div class="uk-margin">
            <label class="uk-form-label" for="form-phone">Номер телефона</label>
            <div class="uk-form-controls">
                <input class="uk-input"
                       id="form-phone"
                       name="phone"
                       type="text" value="{{ $users->phone }}"
                       disabled>
                <div class="alarm-form alarm-true">
                    <img src="/img/correct-symbol.png" alt="correct">
                </div>
                <div class="alarm-form alarm-false">
                    <img src="/img/cancel-mark.png" alt="correct">
                </div>
            </div>
        </div>

        <div class="uk-margin">
            <label class="uk-form-label" for="form-mail">Email</label>
            <div class="uk-form-controls">
                <input class="uk-input"
                       id="form-mail"
                       type="text"
                       name="email"
                       value="{{ $users->email }}">
                <div class="alarm-form alarm-true">
                    <img src="/img/correct-symbol.png" alt="correct">
                </div>
                <div class="alarm-form alarm-false">
                    <img src="/img/cancel-mark.png" alt="correct">
                </div>
            </div>
        </div>

        <div class="uk-margin">
            <label class="uk-form-label" for="form-pass">Пароль</label>
            <div class="uk-form-controls">
                <input class="uk-input"
                       id="form-pass"
                       name="password"
                       type="password"
                >
                <div class="alarm-form alarm-true">
                    <img src="/img/correct-symbol.png" alt="correct">
                </div>
                <div class="alarm-form alarm-false">
                    <img src="/img/cancel-mark.png" alt="correct">
                </div>
            </div>
        </div>

        <div class="uk-margin">
            <label class="uk-form-label" for="form-pass-repeat">Повторите</label>
            <div class="uk-form-controls">
                <input class="uk-input"
                       id="form-pass-repeat"
                       type="password"
                       name="password_confirmation"
                >
                <div class="alarm-form alarm-true">
                    <img src="/img/correct-symbol.png" alt="correct">
                </div>
                <div class="alarm-form alarm-false">
                    <img src="/img/cancel-mark.png" alt="correct">
                </div>
            </div>
        </div>


        <button>ИЗМЕНИТЬ</button>

    </div>
</form>

