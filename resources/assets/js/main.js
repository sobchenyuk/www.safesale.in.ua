window.$ = window.jQuery = require('jquery');
require('./vendor/modernizr-3.5.0.min.js');
require('./uikit.min.js');
require('./plugins.js');
require('./css/style.css');
require('jquery.inputmask/dist/jquery.inputmask.bundle.js');



window.addEventListener('load', function () {
    $('[id*=modal] form').submit(function(e) {
        let form = $(this),
            reg = new RegExp('^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$', 'i'),
            error=0, error1=0;

        if($('input#password', form).val() === "") {
            $('input#password', form).addClass('error');
            alert('Поле пароль не может быть пустым');
            error=0;
        }else {
            $('input#password', form).removeClass('error');
            error=1;
        }

        if($('input#email', form).val() === ""){
            $('input#email', form).addClass('error');
            alert('Поле email  не может быть пустым');
            error1=0;
        }else if(!reg.test($('input#email', form).val())) {
            $('input#email', form).addClass('error');
            alert('Некорректно указан email');
            error1=0;
        }else {
            $('input#email', form).removeClass('error');
            error1=1;
        }

        if( error === 1 && error1 === 1){
            return true
        }else {
            return false
        }

    });
});

(() => {
    $('#refresh').on('click',function(e){
        e.preventDefault();
        let captcha = $('img.captcha-img');
        let config = captcha.data('refresh-config');
        $.ajax({
            method: 'GET',
            url: '/get_captcha/' + config,
        }).done(function (response) {
            captcha.prop('src', response);
        });
    });
})();


$(document).ready(function(){
    $("#registerForm").find("#phone").inputmask({"mask": "(999) 999-9999"}); //mask with dynamic syntax
});